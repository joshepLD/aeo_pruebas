package com.example.aeomovil.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.aeo.utilities.MessageError
import com.example.aeo.utilities.MessageSuccess

class Utils {
    companion object{
        val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"

        fun isEmailValid(email:String):Boolean{
            return EMAIL_REGEX.toRegex().matches(email)
        }

        fun veryfyAvailableNetwork(activity: AppCompatActivity):Boolean{
            val connectivityManager =
                activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }

        fun messageError(context: Context, errores: MessageError) {
            var msg = ""
            when (errores) {
                MessageError.VALIDA_RED -> {
                    msg = "No cuenta con conexion a internet"
                }
                MessageError.DATOS_INVALDO -> {
                    msg = "Datos invalidos"
                }
                MessageError.CONTRASEÑA_INCORECTA -> {
                    msg = "usuario o contraseña incorrecta"
                }
                MessageError.DISPOSITIVO_NO_REGISTRADO -> {
                    msg = "Dipositivo no registrado"
                }
                MessageError.VALIDA_CAMPOS -> {
                    msg = "Los campos estan vacios"
                }
            }
            showToas(context, msg)
        }


        fun messageSuccess(context: Context, ok: MessageSuccess) {
            var msg = ""
            when (msg) {

            }
            showToas(context, msg)
        }

        fun showToas(context: Context, msg: String) {
            val toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        }

    }
}