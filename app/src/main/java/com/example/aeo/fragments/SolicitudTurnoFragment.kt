package com.example.aeo.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.example.aeo.R
import com.example.aeo.model.response.VerificarTurnoResponse
import com.example.aeomovil.model.response.TurnoResponse
import com.example.aeomovil.model.response.ValidarTurnoResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.fragment_solicitud_turno.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime

/**
 * A simple [Fragment] subclass.
 */
class SolicitudTurnoFragment : Fragment() {

    private lateinit var idOperador: String
    private lateinit var idSmart: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_solicitud_turno, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        lyIncorrecto.visibility = View.GONE
        lyCorrecto.visibility = View.GONE
        lyBotom.visibility = View.GONE

        val  sharedPreference = this.activity?.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = sharedPreference?.getString("id_operador","").toString()
        idSmart = sharedPreference?.getString("id_smartphone", "").toString()

        Log.e("Tag", "Solicitar Turno -->"+idOperador)

        searchTurno(idOperador)

        btnConfirmarTurno.setOnClickListener {
            val fech = LocalDateTime.now()


            val dateToISOString = fech.toString()
            println(dateToISOString)


            //FORMATTER.format(fech);

            Log.d("TAG", "Fecha---- "+ fech)
            //Log.d("TAG", "Fecha---- "+ date)

            validarTurno(fech)
        }


    }

    private fun searchTurno(id: String){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).solicitudTurno(id).execute()
            val response = call.body() as TurnoResponse

            uiThread {
                Log.e("Call", ""+call)
                Log.e("Tag", response.toString())

                if(response.estatus == "true"){
                    Log.e("Tag", "No puedes solictar turno ")
                    lyIncorrecto.visibility = View.VISIBLE
                    lyCorrecto.visibility = View.GONE
                    lyBotom.visibility = View.GONE

                    tvMensajeReponse.text = getString(R.string.title_carta_asignada)
                    tvTituloUno.text = getText(R.string.fecha_registro)
                    tvContenidoUno.text = response.datosCI.fechaRegistro

                    tvTituloDos.text = getText(R.string.folio_registro_viaje)
                    tvContenidoDos.text = response.datosCI.folioViaje

                    tvTituloTres.text = getText(R.string.folio_registro_carta)
                    tvContenidoTres.text = response.datosCI.estatusCarta

                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    requestManager?.load(R.drawable.ic_deny_icon)?.into(ivEstatus)

                }else{
                    verificarTurno(idOperador)
                    /*Log.e("Tag", "Puedes solictar turno ")
                    lyIncorrecto.visibility = View.GONE
                    lyCorrecto.visibility = View.VISIBLE
                    lyBotom.visibility = View.VISIBLE

                    tvMensajeReponseC.text = getString(R.string.title_carta_solicitar)
                    tvContenidoUnoC.text = getString(R.string.mensaje_carta_solicitar)

                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    requestManager?.load(R.drawable.correcto)?.into(ivEstatusC)
                    /*
                     if (requestBuilder != null) {
                        requestBuilder.into(ivEstatusC)
                    }*/
                    */
                    /*val requestManager = Glide.with(this@TurnoActivity)
                    val requestBuilder = requestManager.load(R.drawable.correcto)
                    requestBuilder.into(ivEstatus)*/
                }
            }
        }
    }

    private fun validarTurno(fecha:LocalDateTime){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).validarTurno(fecha, idOperador, idSmart).execute()
            val response = call.body() as ValidarTurnoResponse
            uiThread {
                //if(response.estatus == "true"){
                    Log.e("Tag", response.estatus)
                    Log.e("Tag", response.mensaje)
                    val builder = AlertDialog.Builder(context)
                    // Set the alert dialog title
                    builder.setTitle("Turno")
                    // Display a message on alert dialog
                    builder.setMessage(response.mensaje)
                    // Set a positive button and its click listener on alert dialog
                    builder.setPositiveButton("Ok"){dialog, which ->
                    }
                    // Finally, make the alert dialog using builder
                    val dialog: AlertDialog = builder.create()
                    // Display the alert dialog on app interface
                    dialog.show()
                //}
            }
        }
    }

    private fun verificarTurno(id: String){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).turno(id).execute()
            val response = call.body() as VerificarTurnoResponse
            uiThread {
                Log.e("Tag","--->"+response.request)
                Log.e("Call","--->"+call)
                if(response.request == "flase"){
                    Log.e("Tag", "Puedes solictar turno ")
                    lyIncorrecto.visibility = View.GONE
                    lyCorrecto.visibility = View.VISIBLE
                    lyBotom.visibility = View.VISIBLE

                    tvMensajeReponseC.text = getString(R.string.title_carta_solicitar)
                    tvContenidoUnoC.text = getString(R.string.mensaje_carta_solicitar)

                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    requestManager?.load(R.drawable.ic_check_icon)?.into(ivEstatusC)
                    /*
                     if (requestBuilder != null) {
                        requestBuilder.into(ivEstatusC)
                    }*/
                }else{
                    Log.e("Tag", "No Puedes solictar turno ")
                    lyIncorrecto.visibility = View.GONE
                    lyCorrecto.visibility = View.VISIBLE
                    //lyBotom.visibility = View.VISIBLE

                    tvMensajeReponseC.text = getString(R.string.title2_carta_solicitar)
                    tvContenidoUnoC.text = "La fecha de su solicitud de turno es: "
                    tvContenidoDosC.text = response.fecha
                    val requestManager = context?.let { it1 -> Glide.with(it1) }
                    requestManager?.load(R.drawable.warning_act)?.into(ivEstatusC)
                }

            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
