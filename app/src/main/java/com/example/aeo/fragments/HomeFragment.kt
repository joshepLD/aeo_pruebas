package com.example.aeo.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import com.bumptech.glide.Glide

import com.example.aeo.R
import com.example.aeo.activities.SCPMobileApp
import com.example.aeo.model.response.EnvioGaleriaResponseB
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_solicitud_turno.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private lateinit var nombre: String
    private lateinit var apellidoP: String

    private lateinit var idCarta: String
    private lateinit var tipoCI: String
    private lateinit var vistaDetallada: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val  sharedPreference = this.activity?.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        nombre = sharedPreference?.getString("usuario","").toString()
        apellidoP = sharedPreference?.getString("apellidoP","").toString()

        tvHomeName.text = "${nombre} ${apellidoP}"

        /*doAsync {
            var galeria_envio = SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagenAll()
            var datos_ci = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
            var datos_usuario = SCPMobileApp.tablaUsuario.usuario().getUsuario()

            var id_operador = "4"//datos_usuario[0].id_operador
            var id_carta = datos_ci[0].id_carta
            var folio_carta = datos_ci[0].folio
            var tipo_carta = datos_ci[0].operacion

            onComplete {
                //try {
                //val connectivityManager = this.activity?.getSystemService(AppCompatActivity.CONNECTIVITY_SERVICE) as ConnectivityManager
                //val networkInfo  = connectivityManager.activeNetworkInfo

                //if (networkInfo != null && networkInfo.isConnected) {
                    // Si hay conexión a Internet en este momento
                    Log.e("Tag---> ","Si hay conexión a Internet en este momento")

                    Log.v("Consulta", galeria_envio.toString())
                    for (i in 0 until  galeria_envio.size) {
                        var tipo_evidencia = galeria_envio[i].tipo_evidencia
                        var id_evidencia = galeria_envio[i].id_evidencia
                        var comentario_imagen = galeria_envio[i].comentario_imagen
                        var imagen = galeria_envio[i].imagen
                        var base64 = "data:image/jpge:base64,${imagen}"


                        /*val arrayInputStream = ByteArrayInputStream(imagen)
                        val bitmap: Bitmap = BitmapFactory.decodeStream(arrayInputStream)*/

                        Log.e("Envio:", id_operador)
                        Log.e("Envio:", id_carta.toString())
                        Log.e("Envio:", folio_carta)
                        Log.e("Envio:", tipo_carta)
                        Log.e("Envio:", tipo_evidencia)
                        Log.e("Envio:", id_operador)
                        Log.e("Envio:", imagen)
                        Log.e("Envio:", comentario_imagen)

                        //envioGaleria(id_operador, id_carta.toString(), folio_carta, tipo_carta, tipo_evidencia, id_evidencia, base64, comentario_imagen)
                        Log.v("Bitmap Dos", "")
                    }
                //} else {
                    // No hay conexión a Internet en este momento
                    Log.e("Tag---> ","No hay conexión a Internet en este momento")
                //}
                //} catch (e: IOException) {
                //  Log.v("catch", "Hubo un error")
                //}
            }
        }*/

    }

    private fun envioGaleria(
        idOperardor: String,
        idCarta: String,
        folioCarta: String,
        tipoCarta: String,
        tipoEvidencia: String,
        idEvidencia: String,
        evidence: String,
        comentario: String
    )
    {
        Log.e("Tag", "Hola search CI")
        doAsync {
            val call = getRetrofit().create(APIService::class.java)
                .envioGaleriaB(idOperardor, idCarta, folioCarta, tipoCarta, tipoEvidencia, idEvidencia, evidence, "base64",comentario).execute()
            val response = call.body() as EnvioGaleriaResponseB
            Log.e("Service",call.toString())
            Log.e("Service",response.toString())
            uiThread {
                Log.e("Response",response.typeProccess)
                Log.e("Response Data",response.data)
                if(response.typeProccess == "success"){
                    Toast.makeText(context, "Datos Enviados", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
