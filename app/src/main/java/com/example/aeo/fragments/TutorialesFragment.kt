package com.example.aeo.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.adapter.TutorialesAdapter
import com.example.aeo.model.response.TutorialesResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.fragment_tutoriales.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A simple [Fragment] subclass.
 */
class TutorialesFragment : Fragment() {

    private lateinit var linerLayout: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var idOperador: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_tutoriales, container, false)
        recyclerView = view.findViewById(R.id.rvTutoriales)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val  sharedPreference = this.activity?.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = sharedPreference?.getString("id_operador","").toString()

        Log.e("Tag","Holaaaaaaaaa")
        searchTutoriales(idOperador)
    }

    private  fun searchTutoriales(id: String){
        doAsync {
            val call  = getRetrofit().create(APIService::class.java).tutoriales(id).execute()
            Log.e("Tag", call.toString())
            val response = call.body() as TutorialesResponse
            uiThread {
                Log.e("Tag", response.numeroT.toString())
                if (response.numeroT == 0){
                    tvMensajeT.visibility = View.VISIBLE
                    tvMensajeT.text = getText(R.string.title_mensaje_tutoriales)
                }else{
                    rvTutoriales.visibility = View.VISIBLE
                    tvMensajeT.visibility = View.GONE
                    linerLayout = LinearLayoutManager(context)
                    recyclerView.layoutManager = linerLayout
                    recyclerView.adapter = TutorialesAdapter(context as MainActivity, response.dataT)
                }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


}
