package com.example.aeo.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.activities.SCPMobileApp
import com.example.aeo.adapter.AnticipoAdapter
import com.example.aeo.model.response.AnticiposResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.fragment_anticipo.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A simple [Fragment] subclass.
 */
class AnticipoFragment : Fragment() {

    private lateinit var linerLayout: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var idOperador: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_anticipo, container, false)
        recyclerView = view.findViewById(R.id.rvAnticipos)

        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val  sharedPreference = this.activity?.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = sharedPreference?.getString("id_operador","").toString()
        Log.e("IdOperador ---> ",""+idOperador)
        searchAnticipo(idOperador)

        doAsync {
             val consualta = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
             for (i in 0 until consualta.size){
                 val cliente = consualta[i].nombre_cliente_carga
                 val folio = consualta[i].folio
                 val idCarta = consualta[i].id_carta
                 Log.e("CLIENTEDAta","Consulta:  $cliente")
                 Log.e("CLIENTEDAta","$folio")
                 Log.e("CLIENTEDAta","$idCarta")
             }
        }

        doAsync {
            val consualtaDos = SCPMobileApp.tablaInicioCi.inicio_ci().getAllInicio()
            for (i in 0 until consualtaDos.size){
                val datos = consualtaDos[i].datos
                val nombre = consualtaDos[i].nombre
                val direccion = consualtaDos[i].direccion
                Log.e("CLIENTEDAta","ConsultaDos:  $datos")
                Log.e("CLIENTEDAta","$nombre")
                Log.e("CLIENTEDAta","$direccion")
            }
        }

       /* doAsync {
            val consualtaTres = SCPMobileApp.tablaLugarCargaCi.lugar_carga_ci().getAllLugarCarga()
            for (i in 0 until consualtaTres.size){
                val id_cart = consualtaTres[i].id_carta
                val direccionT = consualtaTres[i].direccion
                Log.e("CLIENTEDAta","ConsultaTres$id_cart")
                Log.e("CLIENTEDAta","$direccionT")
            }
        }

        doAsync {
            val consualtaTres = SCPMobileApp.tablaLugarDescargaCi.lugar_descarga_ci().getAllLugarDescarga()
            for (i in 0 until consualtaTres.size){
                val id_cart = consualtaTres[i].id_carta
                val direccionT = consualtaTres[i].direccion
                Log.e("CLIENTEDAta","ConsultaTres$id_cart")
                Log.e("CLIENTEDAta","$direccionT")
            }
        }*/
    }

    private fun searchAnticipo(id: String){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).anticipos(id).execute()
            val response = call.body() as AnticiposResponse

            uiThread {
                if(response.advancePayment == "true"){
                    rvAnticipos.visibility = View.VISIBLE
                    tvMensajeA.visibility = View.GONE
                    linerLayout = LinearLayoutManager(context)
                    recyclerView.layoutManager = linerLayout
                    recyclerView.adapter = AnticipoAdapter(context as MainActivity, response.data)
                }else{
                    tvMensajeA.visibility = View.VISIBLE
                    tvMensajeA.text = getText(R.string.title_mensaje_liquidacion)
                }

            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
