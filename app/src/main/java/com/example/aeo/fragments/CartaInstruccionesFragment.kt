package com.example.aeo.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.diegodobelo.expandingview.ExpandingItem
import com.diegodobelo.expandingview.ExpandingList
import com.example.aeo.R
import com.example.aeo.activities.EvidenciasActivity
import com.example.aeo.activities.LoginActivity
import com.example.aeo.activities.MapasActivity
import com.example.aeo.activities.SCPMobileApp
import com.example.aeo.entity.*
import com.example.aeo.model.models.DataCI
import com.example.aeo.model.response.LeidoResponse
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.task.APIService
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_carta_instrucciones.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * A simple [Fragment] subclass.
 */
class CartaInstruccionesFragment : Fragment() {
    private lateinit var mExpandingList: ExpandingList
    private lateinit var linearLayout: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var idOperador: String

    private var tipo_leido: String = "0"
    private lateinit var idCarta: String
    private lateinit var tipoCI: String
    private lateinit var vistaDetallada: String

    private  var statusListDetalles = false

    private lateinit var id_carta: String
    lateinit var tasks: List<tb_carta_instruccion>
    //lateinit var adapter:

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_carta_instrucciones, container, false)
        mExpandingList = view.findViewById(R.id.expanding_list_main)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val sharedPreference = this.activity!!.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = "5"//sharedPreference.getString("id_operador","").toString()
        Log.e("Tag", idOperador)
        searchCI(idOperador)

        doAsync {
            val consualta_carta_instruccion = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
            for (i in 0 until consualta_carta_instruccion.size){
                idCarta = consualta_carta_instruccion[i].id_carta.toString()
                tipoCI = consualta_carta_instruccion[i].operacion_abrv
                vistaDetallada = consualta_carta_instruccion[i].vista_detallada
                Log.e("CLIENTEDAta","ConsultaCI:  $idCarta")
                Log.e("CLIENTEDAta","$tipoCI")
                Log.e("CLIENTEDAta","$vistaDetallada")
            }
        }

        lyDescripcion.setOnClickListener {
            if (statusListDetalles == false){
                icluideList.visibility = View.VISIBLE
                statusListDetalles = true
            }else{
                icluideList.visibility = View.INVISIBLE
                statusListDetalles = false
            }
            /*if (vistaDetallada == "N/A"){
                Log.e("Tag", "CI Detallada no habia sido leida")
                var fech = LocalDateTime.now()
                var fechR = fech.toString()
                var fechRP = fechR.replace("T"," ")

                val dateToISOString = fech.toString()
                println(dateToISOString)
                //FORMATTER.format(fech);
                Log.d("TAG", "Fecha----> "+ fechR)
                Log.d("TAG", "Fecha----> "+ fechRP)
                //Log.d("TAG", "Fecha---- "+ date)
                tipo_leido = "1"
                leidoCI(idCarta, tipoCI, fechRP, tipo_leido)
            }else{
                Log.e("Tag", "CI Detallada ya fue leida")
            }*/
        }
        lyMapas.setOnClickListener {
            val intent = Intent(activity, MapasActivity::class.java)
            startActivity(intent)

        }
        lyEvidencias.setOnClickListener {
            val intent = Intent(activity, EvidenciasActivity::class.java)
            startActivity(intent)

        }

        /*val fechePrueba = "2020-05-29 13:20:00"

        val date = LocalDate.parse(fechePrueba, DateTimeFormatter.ISO_DATE_TIME)

        Log.e("Convert Fecha -->", date.toString())*/

    }

    private fun searchCI(id: String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).cartaInstrucciones(id).execute()
            val response = call.body() as CartaInstruccionesResponse
            uiThread {
                //if (call.isSuccessful){
                if (response?.TypeProccess == "success") {

                    deleteData(response.dataCI)
                    tvMensajeCI.visibility = View.GONE
                    lyCartaInstrucciones.visibility = VISIBLE
                    lyCILeida.visibility = VISIBLE
                    tvCiUno.text = response.dataCI.ci.nombre_cliente_carga
                    //tvCiDos.text = response.data[0].lugarCarga.nombre
                    tvCiTres.text = response.dataCI.ci.horarioCarga

                    var remolques = response.dataCI
                    createItems(remolques)

                    if (response.dataCI.ci.vista_general == "N/A"){
                        Log.e("Tag", "CI Aun no habia sido vista")
                        var fech = LocalDateTime.now()
                        var fechR = fech.toString()
                        var fechRP = fechR.replace("T"," ")

                        val dateToISOString = fech.toString()
                        println(dateToISOString)
                        //FORMATTER.format(fech);
                        Log.d("TAG", "Fecha----> "+ fechR)
                        Log.d("TAG", "Fecha----> "+ fechRP)
                        //Log.d("TAG", "Fecha---- "+ date)
                        var idCartaG = response.dataCI.ci.idCarta
                        var tipoCIG = response.dataCI.ci.operacion_abrv
                        tipo_leido = "0"
                        leidoCI(idCartaG, tipoCIG, fechRP, tipo_leido)
                    }else{
                        Log.e("Tag", "CI ya fue leida")
                    }

                } else {
                    tvMensajeCI.visibility = VISIBLE
                    toasMessage("No se inserta datos")
                }
                //}else{
                //  toasMessage("Ocurrio un error intente otra vez")
                //}
            }
        }
    }

    private fun leidoCI(idCarta:String, tipoCI:String, fechRP:String, tipo_leido:String) {
        Log.e("Leido CI ::", tipo_leido)
        doAsync {
            val call = getRetrofit().create(APIService::class.java).LeidoCI(idCarta, tipoCI, fechRP, tipo_leido).execute()
            val response = call.body() as LeidoResponse
            uiThread {
                Log.e("Leido CI ::", response.type_proccess)
                Log.e("Leido CI ::", response.dataL)
            }
        }
    }

    private fun deleteData(paramData: DataCI) {
        doAsync {
             SCPMobileApp.tablaCartaInstruccion.carta_instruccion().deleteCI()
            onComplete {
                inserData(paramData)
            }
        }
    }

    private fun inserData(paramData: DataCI) {
        doAsync {
            /*for (i in 0 until paramData.size) {
                var id_carta = paramData[i].idCarta.toInt()
                var folio = paramData[i].folio
                var operacion = paramData[i].operacion
                var operacion_abrv = paramData[i].operacion_abrv
                var tipo_servicio = paramData[i].tipoServicio
                var tipo_viaje = paramData[i].tipoViaje
                var placa = paramData[i].placa
                var eco_camion = paramData[i].eco_camion
                var nombre_cliente_carga = paramData[i].nombre_cliente_carga
                var cliente_paga = paramData[i].clientePaga
                var numero_viaje = paramData[i].numeroViaje
                var carta_porte = paramData[i].cartaPorte
                var nombre_contacto_carga = paramData[i].nombreContactoCarga
                var tel_contacto_carga = paramData[i].telContactoCarga
                var horario_inicio = paramData[i].horarioInicio
                var horario_carga = paramData[i].horarioCarga
                var horario_descarga = paramData[i].horarioDescarga
                var maniobras = paramData[i].maniobras
                var observaciones = paramData[i].observaciones
                var tipo_inicio = paramData[i].tipoInicio
                var evidencias_observaciones = paramData[i].evidenciasObservaciones
                var vista_gral = paramData[i].vista_general
                var vista_detallada = paramData[i].vista_detallada

                var inicio_data = paramData[i].inicio
                var remolques_data = paramData[i].remolques
                var ruta_data = paramData[i].ruta
                var lugar_carga = paramData[i].lugarCarga
                var lugar_descarga = paramData[i].lugarDescarga
                //var evidencias_observaciones = paramData[i].ev


                var carta = tb_carta_instruccion(
                    id_carta,
                    folio,
                    operacion,
                    operacion_abrv,
                    tipo_servicio,
                    tipo_viaje,
                    placa,
                    eco_camion,
                    nombre_cliente_carga,
                    cliente_paga,
                    numero_viaje,
                    carta_porte,
                    nombre_contacto_carga,
                    tel_contacto_carga,
                    horario_inicio,
                    horario_carga,
                    horario_descarga,
                    maniobras,
                    observaciones,
                    tipo_inicio,
                    evidencias_observaciones,
                    vista_gral,
                    vista_detallada
                )

                var gson_inicio = Gson()
                var json_inicio = gson_inicio.toJson(inicio_data)
                var jsonObject_inicio = JSONObject(json_inicio)
                //var objectArray: JSONArray = jsonObject.getJSONArray("")
                //Log.e("JSON",""+jsonObject_inicio)

                var gson_remolques = Gson()
                var json_remolques = gson_remolques.toJson(remolques_data)
                var jsonObject_remolques = JSONObject(json_remolques)
                //var objectArray: JSONArray = jsonObject.getJSONArray("")
                //Log.e("JSON",""+jsonObject_remolques)

                var gson_ruta = Gson()
                var json_ruta = gson_ruta.toJson(ruta_data)
                var jsonObject_ruta = JSONObject(json_ruta)
                //var objectArray: JSONArray = jsonObject.getJSONArray("")
                //Log.e("JSON",""+jsonObject_ruta)

                SCPMobileApp.tablaCartaInstruccion.carta_instruccion().addCI(carta)

                var inicio_datos = jsonObject_inicio.get("datos").toString()
                var inicio_nombre = jsonObject_inicio.get("nombre").toString()
                var inicio_direccion = jsonObject_inicio.get("direccion").toString()
                var inicio_tipo = jsonObject_inicio.get("tipo").toString()
                //var Coordenadas = inicioData.coordenadas

                var inicio =  tb_inicio_ci(
                    id_carta,
                    inicio_datos,
                    inicio_nombre,
                    inicio_direccion,
                    inicio_tipo
                )
                SCPMobileApp.tablaInicioCi.inicio_ci().addInicio(inicio)


                var ruta_datos = jsonObject_ruta.get("datos").toString()
                var ruta_nombre = jsonObject_ruta.get("nombre").toString()
                var ruta_alias = jsonObject_ruta.get("alias").toString()
                var ruta_casetas = jsonObject_ruta.getJSONObject("casetas")

                //Log.e("RutaCasetas:  ",""+ruta_casetas)

                var ruta_casetas_datos = ruta_casetas.get("datos").toString()
                //Log.e("ruta_casetas_datos",""+ruta_casetas_datos)

                var ruta = tb_ruta_ci(
                    id_carta,
                    ruta_datos,
                    ruta_nombre,
                    ruta_alias
                )

                SCPMobileApp.tablaRutaCi.ruta_ci().addRuta(ruta)

                var remolques_datos = jsonObject_remolques.get("datos").toString()
                //Log.e("JSON.Datos",""+remolques_datos)

                if (remolques_datos == "Si"){
                    //Log.e("JSON.Datos.R","Siiiii entre")
                    val remolques_resultados = jsonObject_remolques.getJSONArray("resultados")
                    //Log.e("JSON.Array",""+remolques_resultados)
                    for (a in 0 until remolques_resultados.length()){
                        val sObjectRemolques: JSONObject = remolques_resultados.getJSONObject(a)
                        val list_placa = sObjectRemolques.get("placa").toString()
                        val list_economico = sObjectRemolques.get("economico").toString()
                        val idT = a+1
                        val remolques = tb_remolques_ci(
                            idT,
                            id_carta,
                            list_placa,
                            list_economico
                        )
                        SCPMobileApp.tablaRemolquesCi.remolques_ci().addRemolques(remolques)
                        //Log.e("JSON_remolques_placa",list_placa)
                        //Log.e("JSON_remolques_economico",list_economico)
                    }
                }

                if (ruta_casetas_datos == "Si"){
                    var ruta_casetas_resultado = ruta_casetas.getJSONArray("resultado")
                    for (b in 0 until ruta_casetas_resultado.length()){
                        val sObjectCasetas: JSONObject = ruta_casetas_resultado.getJSONObject(b)
                        val list_nombre = sObjectCasetas.get("nombre").toString()
                        val list_alias = sObjectCasetas.get("alias").toString()

                        val casetas = tb_casetas_ci(
                            id_carta,
                            list_nombre,
                            list_alias
                        )
                        //Revisar las inserciones de casetas
                        SCPMobileApp.tablaCasetasCi.casetas_ci().addCasetas(casetas)
                        Log.e("list_nombre",list_nombre)
                        Log.e("list_alias",list_alias)
                    }
                }

                   /*for (o in 0 until lugar_carga.size){
                      var DatosLC = lugar_carga[o].datos
                      var NombreLC = lugar_carga[o].nombre
                      var DireccionLC = lugar_carga[o].direccion

                      var lugarCarga =  tb_lugar_carga_ci(
                          id_carta,
                          DatosLC,
                          NombreLC,
                          DireccionLC
                      )
                      SCPMobileApp.tablaLugarCargaCi.lugar_carga_ci().addLugarCarga(lugarCarga)

                      for (u in 0 until lugar_descarga.size){
                          var DatosLD = lugar_descarga[u].datos
                          var NombreLD = lugar_descarga[u].nombre
                          var DireccionLD = lugar_descarga[u].direccion

                          var lugarDescarga =  tb_lugar_descarga_ci(
                              id_carta,
                              DatosLD,
                              NombreLD,
                              DireccionLD
                          )
                          SCPMobileApp.tablaLugarDescargaCi.lugar_descarga_ci().addLugarDescarga(lugarDescarga)

                          /*for (i in 0 until paramData.size){
                              var idCartaLD = paramData[i].idCarta.toInt()
                              var DatosLD = paramData[i].lugarDescarga.datos
                              var NombreLD = paramData[i].lugarDescarga.nombre
                              var DireccionLD = paramData[i].lugarDescarga.direccion

                              var lugarDescarga =  tb_lugar_descarga_ci(
                                  idCartaLD,
                                  DatosLD,
                                  NombreLD,
                                  DireccionLD
                              )
                              SCPMobileApp.tablaLugarDescargaCi.lugar_descarga_ci().addLugarDescarga(lugarDescarga)
                          }*/
                      }
                  }*/
            }
            onComplete {
                Toast.makeText(context, "Datos insertados correctamente", Toast.LENGTH_LONG).show()
                Log.e("InsertData", "Datos Insertados")
            }*/
        }
    }

    private fun createItems(subItems: DataCI){
        addItem("Camión",
            arrayOf(
                "Economico: ",
                "Placa: "
            ),
            arrayOf(
                subItems.ci.eco_camion,
                subItems.ci.placa

            ),
            R.color.colorAzulC, R.drawable.ic_galery_truck)

        addItem("semiremolque(s)",
            arrayOf(
                "Económico: ",
                "Placa: ",
                "Area: "
            ),

            arrayOf(
                subItems.ci.remolques.resultados[0].economico,
                subItems.ci.remolques.resultados[0].placa,
                subItems.ci.remolques.resultados[0].area
            ),
            R.color.colorAzulC, R.drawable.ic_galery_remolque)

        addItem("Viaje",
            arrayOf(
                "Tipo: ",
                "Operación: ",
                "Maniobras: ",
                "Lugar Destino: "
            ),
            arrayOf(
                subItems.ci.tipoViaje,
                subItems.ci.operacion,
                subItems.ci.maniobras
//                subItems[0].lugarDescarga.nombre
            ),
            R.color.colorAzulC, R.drawable.ic_galery_carretera)

        addItem("Contacto",
            arrayOf(
                "Nombre cliente carga: ",
                "Contacto: ",
                "Telefono: "
            ),
            arrayOf(
                subItems.ci.nombre_cliente_carga,
                subItems.ci.nombreContactoCarga,
                subItems.ci.telContactoCarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_contacto)

        addItem("Horarios",
            arrayOf(
                "Inicio: ",
                "Carga: ",
                "Descarga: "
            ),
            arrayOf(
                subItems.ci.horarioInicio,
                subItems.ci.horarioCarga,
                subItems.ci.horarioDescarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_reloj)

    }

    /*private fun createItemsDos(subItems: DataCI){
        addItem("Camión",
            arrayOf(
                "Economico: ",
                "Placa: "
            ),
            arrayOf(
                subItems.ci.eco_camion,
                subItems.ci.placa

            ),
            R.color.colorAzulC, R.drawable.ic_galery_truck)

        addItem("semiremolque (s)",
            arrayOf(
                "Económico: ",
                "Placa: ",
                "Area: "
            ),

            arrayOf(
                subItems[0].remolques.resultados[0].economico,
                subItems[0].remolques.resultados[0].placa,
                subItems[0].remolques.resultados[0].area
            ),
            R.color.colorAzulC, R.drawable.ic_galery_remolque)

        addItem("Viaje",
            arrayOf(
                "Tipo: ",
                "Operación: ",
                "Maniobras: ",
                "Lugar Destino: "
            ),
            arrayOf(
                subItems[0].tipoViaje,
                subItems[0].operacion,
                subItems[0].maniobras,
                subItems[0].lugarDescarga.nombre
            ),
            R.color.colorAzulC, R.drawable.ic_galery_carretera)

        addItem("Contacto",
            arrayOf(
                "Cliente: ",
                "Contacto: ",
                "Telefono: "
            ),
            arrayOf(
                subItems[0].nombre_cliente_carga,
                subItems[0].nombreContactoCarga,
                subItems[0].telContactoCarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_contacto)

        addItem("Horarios",
            arrayOf(
                "Inicio: ",
                "Carga: ",
                "Descarga: "
            ),
            arrayOf(
                subItems[0].horarioInicio,
                subItems[0].horarioCarga,
                subItems[0].horarioDescarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_reloj)

        addItem("Caseta (s)",
            arrayOf(
                "Nombre: ",
                "Alias: "
            ),

            arrayOf(
                subItems[0].ruta.casetas.resultado_casetas[0].nombre_caseta,
                subItems[0].ruta.casetas.resultado_casetas[0].alias_caseta
            ),
            R.color.colorAzulC, R.drawable.ic_galery_caseta)
    }

    private fun createItemsTres(subItems: ArrayList<DataCI>){
        addItem("Camión",
            arrayOf(
                "Economico: ",
                "Placa: "
            ),
            arrayOf(
                subItems[0].eco_camion,
                subItems[0].placa

            ),
            R.color.colorAzulC, R.drawable.ic_galery_truck)

        addItem("Viaje",
            arrayOf(
                "Tipo: ",
                "Operación: ",
                "Maniobras: ",
                "Lugar Destino: "
            ),
            arrayOf(
                subItems[0].tipoViaje,
                subItems[0].operacion,
                subItems[0].maniobras,
                subItems[0].lugarDescarga.nombre
            ),
            R.color.colorAzulC, R.drawable.ic_galery_carretera)

        addItem("Contacto",
            arrayOf(
                "Cliente: ",
                "Contacto: ",
                "Telefono: "
            ),
            arrayOf(
                subItems[0].nombre_cliente_carga,
                subItems[0].nombreContactoCarga,
                subItems[0].telContactoCarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_contacto)

        addItem("Horarios",
            arrayOf(
                "Inicio: ",
                "Carga: ",
                "Descarga: "
            ),
            arrayOf(
                subItems[0].horarioInicio,
                subItems[0].horarioCarga,
                subItems[0].horarioDescarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_reloj)

        addItem("Caseta (s)",
            arrayOf(
                "Nombre: ",
                "Alias: "
            ),

            arrayOf(
                subItems[0].ruta.casetas.resultado_casetas[0].nombre_caseta,
                subItems[0].ruta.casetas.resultado_casetas[0].alias_caseta
            ),
            R.color.colorAzulC, R.drawable.ic_galery_caseta)
    }

    private fun createItemsCuatro(subItems: ArrayList<DataCI>){
        addItem("Camión",
            arrayOf(
                "Economico: ",
                "Placa: "
            ),
            arrayOf(
                subItems[0].eco_camion,
                subItems[0].placa

            ),
            R.color.colorAzulC, R.drawable.ic_galery_truck)

        addItem("Viaje",
            arrayOf(
                "Tipo: ",
                "Operación: ",
                "Maniobras: ",
                "Lugar Destino: "
            ),
            arrayOf(
                subItems[0].tipoViaje,
                subItems[0].operacion,
                subItems[0].maniobras
//                subItems[0].lugarDescarga.nombre
            ),
            R.color.colorAzulC, R.drawable.ic_galery_carretera)

        addItem("Contacto",
            arrayOf(
                "Cliente: ",
                "Contacto: ",
                "Telefono: "
            ),
            arrayOf(
                subItems[0].nombre_cliente_carga,
                subItems[0].nombreContactoCarga,
                subItems[0].telContactoCarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_contacto)

        addItem("Horarios",
            arrayOf(
                "Inicio: ",
                "Carga: ",
                "Descarga: "
            ),
            arrayOf(
                subItems[0].horarioInicio,
                subItems[0].horarioCarga,
                subItems[0].horarioDescarga
            ),
            R.color.colorAzulC, R.drawable.ic_galery_reloj)
    }*/

    private fun addItem(title: String, subItems2: Array<String>, subItems: Array<String>, colorRes: Int, iconRes: Int) {

        val item = mExpandingList.createNewItem(R.layout.expanding_layout)


        if (item != null) {
            item.setIndicatorIconRes(iconRes)
            item.setIndicatorColorRes(colorRes)

            (item.findViewById<View>(R.id.title) as TextView).text = title

            item.createSubItems(subItems.size)
            for (i in 0 until item.subItemsCount) {
                val view = item.getSubItemView(i)
                configureSubItem(item, view, subItems[i], subItems2[i])
            }
        }
    }

    private fun configureSubItem(item: ExpandingItem, view: View, subTitle: String, titleSub: String) {
        (view.findViewById<View>(R.id.title_sub) as TextView).text = titleSub
        (view.findViewById<View>(R.id.sub_title) as TextView).text = subTitle

    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun toasMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }
}
