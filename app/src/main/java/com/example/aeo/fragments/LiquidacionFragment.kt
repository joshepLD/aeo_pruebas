package com.example.aeo.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.adapter.AnticipoAdapter
import com.example.aeo.adapter.LiquidacionAdapter
import com.example.aeo.model.response.AnticiposResponse
import com.example.aeo.model.response.LiquidacionResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.fragment_liquidacion.*
import kotlinx.android.synthetic.main.fragment_tutoriales.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A simple [Fragment] subclass.
 */
class LiquidacionFragment : Fragment() {

    private lateinit var linearLayout: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var idOperador: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_liquidacion, container, false)
        recyclerView = view.findViewById(R.id.rvLiquidacion)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val  sharedPreference = this.activity?.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = sharedPreference?.getString("id_operador","").toString()

        searchLiquidacion(idOperador)
    }

    private fun searchLiquidacion(id: String){
        doAsync {
            val call = getRetrofit().create(APIService::class.java).liquidacion(id).execute()
            val response = call.body() as LiquidacionResponse

            uiThread {
                if(response.Liquidacion == "true"){
                    rvLiquidacion.visibility = View.VISIBLE
                    tvMensajeL.visibility = View.GONE
                    linearLayout = GridLayoutManager(context, 2)
                    recyclerView.layoutManager = linearLayout
                    recyclerView.adapter = LiquidacionAdapter(context as MainActivity, response.dataLiquidacion)
                }else{
                    tvMensajeL.visibility = View.VISIBLE
                    tvMensajeL.text = getText(R.string.title_mensaje_liquidacion)
                }

            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
