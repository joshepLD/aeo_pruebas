package www.tasisoft.com.db

import androidx.room.*
import com.example.aeo.entity.tb_inicio_ci

@Dao
interface dao_galeria_ci {



    @Insert
    fun insertGaleria(galeria: tb_galeria_ci):Long

    @Query("SELECT * FROM tb_galeria_ci_entity ORDER BY id DESC")
    fun getImagen(): List<tb_galeria_ci>


    @Query("SELECT * FROM tb_galeria_ci_entity where id_evidencia =:id_parametro and tipo_evidencia =:tipo_parametro ORDER BY id DESC")
    fun getImagenId(id_parametro: String, tipo_parametro: String): List<tb_galeria_ci>


    @Query("SELECT * FROM tb_galeria_ci_entity where id_evidencia =:id_parametro and tipo_evidencia =:tipo_parametro  and valor_enviado =:valor ORDER BY id DESC")
    fun getImagenNotSend(id_parametro: String, tipo_parametro: String, valor:Int): List<tb_galeria_ci>

    @Query("SELECT * FROM tb_galeria_ci_entity ORDER BY id DESC")
    fun getImagenAll(): List<tb_galeria_ci>

    //Vallidar con el query el id en lugar de id_evidencias solo para Incidencias
    /*@Query("SELECT * FROM tb_galeria_ci_entity where id_evidencia =:id_parametro and tipo_evidencia =:tipo_parametro ORDER BY id DESC")
    fun getImagenId(id_parametro: String, tipo_parametro: String): List<tb_galeria_ci>*/

    @Query("DELETE FROM tb_galeria_ci_entity")
    fun deleteImagen()

    @Update
    fun updateImagen(taskEntity: tb_galeria_ci):Int

}