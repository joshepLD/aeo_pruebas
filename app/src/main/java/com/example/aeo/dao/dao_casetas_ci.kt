package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_casetas_ci
import com.example.aeo.entity.tb_inicio_ci

@Dao
interface dao_casetas_ci {
    @Query("SELECT * FROM tb_casetas_ci_entity")
    fun getAllCasetas(): List<tb_casetas_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCasetas(taskEntity : tb_casetas_ci):Long

    @Query("DELETE FROM tb_casetas_ci_entity")
    fun deleteCasetas()

    @Update
    fun updateCasetas(taskEntity: tb_casetas_ci):Int
}