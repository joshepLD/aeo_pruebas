package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_descarga_ci
import com.example.aeo.entity.tb_inicio_ci

@Dao
interface dao_inicio_ci {
    @Query("SELECT * FROM tb_inicio_ci_entity")
    fun getAllInicio(): List<tb_inicio_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInicio(taskEntity : tb_inicio_ci):Long

    @Query("DELETE FROM tb_inicio_ci_entity")
    fun deleteInicio()

    @Update
    fun updateInicio(taskEntity: tb_inicio_ci):Int
}