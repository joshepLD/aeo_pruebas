package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_descarga_ci
import com.example.aeo.entity.tb_lugar_descarga_ci

@Dao
interface dao_lugar_descarga_ci {
    @Query("SELECT * FROM tb_lugar_descarga_ci_entity")
    fun getAllLugarDescarga(): List<tb_lugar_descarga_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addLugarDescarga(taskEntity : tb_lugar_descarga_ci):Long

    @Query("DELETE FROM tb_lugar_descarga_ci_entity")
    fun deleteLugarDescarga()

    @Update
    fun updateLugarDescarga(taskEntity: tb_lugar_descarga_ci):Int
}