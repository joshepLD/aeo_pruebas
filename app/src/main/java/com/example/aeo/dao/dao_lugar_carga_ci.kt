package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_descarga_ci
import com.example.aeo.entity.tb_lugar_carga_ci

@Dao
interface dao_lugar_carga_ci {
    @Query("SELECT * FROM tb_lugar_carga_ci_entity")
    fun getAllLugarCarga(): List<tb_lugar_carga_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addLugarCarga(taskEntity : tb_lugar_carga_ci):Long

    @Query("DELETE FROM tb_lugar_carga_ci_entity")
    fun deleteLugarCarga()

    @Update
    fun updateLugarCarga(taskEntity: tb_lugar_carga_ci):Int
}