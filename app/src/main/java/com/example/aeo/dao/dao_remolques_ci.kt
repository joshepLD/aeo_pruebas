package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_casetas_ci
import com.example.aeo.entity.tb_remolques_ci

@Dao
interface dao_remolques_ci {
    @Query("SELECT * FROM tb_remolques_ci_entity")
    fun getAllRemolques(): List<tb_remolques_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addRemolques(taskEntity : tb_remolques_ci):Long

    @Query("DELETE FROM tb_remolques_ci_entity")
    fun deleteRemolques()

    @Update
    fun updateRemolques(taskEntity: tb_remolques_ci):Int
}