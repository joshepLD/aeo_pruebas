package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_ci

@Dao
interface dao_evidencia_ci {
    @Query("SELECT * FROM tb_evidencia_ci_entity")
    fun getAllEvidencia(): List<tb_evidencia_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addEvidencia(taskEntity : tb_evidencia_ci):Long

    @Query("DELETE FROM tb_evidencia_ci_entity")
    fun deleteEvidencia()

    @Update
    fun updateEvidencia(taskEntity: tb_evidencia_ci):Int
}