package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_carta_instruccion


@Dao
interface dao_carta_instruccion {
    @Query("SELECT * FROM tb_carta_instruccion_entity")
    fun getAllCI(): List<tb_carta_instruccion>

    @Insert//(onConflict = OnConflictStrategy.REPLACE)
    fun addCI(taskEntity : tb_carta_instruccion):Long

    @Query("DELETE FROM tb_carta_instruccion_entity")
    fun deleteCI()

    @Update
    fun updateCI(taskEntity: tb_carta_instruccion):Int
}

//