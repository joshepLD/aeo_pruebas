package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_usuario

@Dao
interface dao_usuario {
    @Query("SELECT * FROM tb_usuario_entity")
    fun getUsuario(): List<tb_usuario>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUsuario(taskEntity : tb_usuario):Long

    @Query("DELETE FROM tb_usuario_entity")
    fun deleteUsuario()

    @Update
    fun updateUsuario(taskEntity: tb_usuario):Int
}
