package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_incidencia_ci

@Dao
interface dao_evidencia_incidencia_ci {
    @Query("SELECT * FROM tb_evidencia_incidencia_ci_entity")
    fun getAllIncidencias(): MutableList<tb_evidencia_incidencia_ci>

    @Insert
    fun addIncidencia(taskEntity : tb_evidencia_incidencia_ci):Long

    @Delete
    fun deleteIncidencia(taskEntity: tb_evidencia_incidencia_ci):Int

    @Update
    fun updateIncidencia(taskEntity: tb_evidencia_incidencia_ci):Int
}