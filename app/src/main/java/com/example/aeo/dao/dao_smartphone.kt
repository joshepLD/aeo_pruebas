package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_smartphone
import com.example.aeo.entity.tb_usuario

@Dao
interface dao_smartphone {
    @Query("SELECT * FROM tb_smartphone_entity")
    fun getSmartphone(): List<tb_smartphone>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSmartphone(taskEntity : tb_smartphone):Long

    @Query("DELETE FROM tb_smartphone_entity")
    fun deleteSmartphone()

    @Update
    fun updateSmartphone(taskEntity: tb_smartphone):Int
}
