package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_ruta_ci

@Dao
interface dao_ruta_ci {
    @Query("SELECT * FROM tb_ruta_ci_entity")
    fun getAllRuta(): List<tb_ruta_ci>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addRuta(taskEntity : tb_ruta_ci):Long

    @Query("DELETE FROM tb_ruta_ci_entity")
    fun deleteRuta()

    @Update
    fun updateRuta(taskEntity: tb_ruta_ci):Int
}