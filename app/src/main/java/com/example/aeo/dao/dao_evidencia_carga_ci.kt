package com.example.aeo.dao

import androidx.room.*
import com.example.aeo.entity.tb_evidencia_carga_ci

@Dao
interface dao_evidencia_carga_ci {
    @Query("SELECT * FROM tb_evidencia_carga_ci_entity")
    fun getAllEvidenciasCarga(): MutableList<tb_evidencia_carga_ci>

    @Insert
    fun addEvidenciasCarga(taskEntity : tb_evidencia_carga_ci):Long

    @Delete
    fun deleteEvidenciasCarga(taskEntity: tb_evidencia_carga_ci):Int

    @Update
    fun updateEvidenciasCarga(taskEntity: tb_evidencia_carga_ci):Int
}