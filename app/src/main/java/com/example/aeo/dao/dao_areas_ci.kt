package www.tasisoft.com.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface dao_areas_ci {
    @Query("SELECT * FROM tb_areas_ci_entity ORDER BY id DESC")
    fun getAreas(): List<tb_areas_ci>

    @Insert
    fun insertAreas(areas: tb_areas_ci):Long
}