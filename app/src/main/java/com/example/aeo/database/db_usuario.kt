package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_usuario
import com.example.aeo.entity.tb_usuario

@Database(entities = arrayOf(tb_usuario::class), version =   1)
abstract class db_usuario : RoomDatabase() {
    abstract fun usuario():dao_usuario
}