package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_smartphone
import com.example.aeo.dao.dao_usuario
import com.example.aeo.entity.tb_smartphone
import com.example.aeo.entity.tb_usuario

@Database(entities = arrayOf(tb_smartphone::class), version =   1)
abstract class db_smartphone : RoomDatabase() {
    abstract fun smartphone(): dao_smartphone
}