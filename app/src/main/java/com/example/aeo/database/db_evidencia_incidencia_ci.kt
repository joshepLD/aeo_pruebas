package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_evidencia_incidencia_ci
import com.example.aeo.entity.tb_evidencia_incidencia_ci


@Database(entities = arrayOf(tb_evidencia_incidencia_ci::class), version =   1)
abstract class db_evidencia_incidencia_ci : RoomDatabase() {
    abstract fun incidenciaDao(): dao_evidencia_incidencia_ci
}