package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_inicio_ci
import com.example.aeo.entity.tb_inicio_ci


@Database(entities = arrayOf(tb_inicio_ci::class), version =   1)
abstract class db_inicio_ci : RoomDatabase() {
    abstract fun inicio_ci(): dao_inicio_ci
}