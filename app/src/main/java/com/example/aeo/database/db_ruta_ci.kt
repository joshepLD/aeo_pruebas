package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_ruta_ci
import com.example.aeo.entity.tb_ruta_ci


@Database(entities = arrayOf(tb_ruta_ci::class), version =   1)
abstract class db_ruta_ci : RoomDatabase() {
    abstract fun ruta_ci(): dao_ruta_ci
}