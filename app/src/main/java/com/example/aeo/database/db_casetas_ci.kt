package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_casetas_ci
import com.example.aeo.entity.tb_casetas_ci


@Database(entities = arrayOf(tb_casetas_ci::class), version =   1)
abstract class db_casetas_ci : RoomDatabase() {
    abstract fun casetas_ci(): dao_casetas_ci
}