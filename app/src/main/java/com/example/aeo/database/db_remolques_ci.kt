package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_remolques_ci
import com.example.aeo.entity.tb_remolques_ci


@Database(entities = arrayOf(tb_remolques_ci::class), version =   1)
abstract class db_remolques_ci : RoomDatabase() {
    abstract fun remolques_ci(): dao_remolques_ci
}