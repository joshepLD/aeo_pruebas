package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_carta_instruccion
import com.example.aeo.entity.tb_carta_instruccion

@Database(entities = arrayOf(tb_carta_instruccion::class), version =   1)
abstract class db_carta_instruccion : RoomDatabase() {
    abstract fun carta_instruccion(): dao_carta_instruccion
}