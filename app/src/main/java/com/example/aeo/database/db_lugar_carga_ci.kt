package com.example.aeo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aeo.dao.dao_lugar_carga_ci
import com.example.aeo.entity.tb_lugar_carga_ci

@Database(entities = arrayOf(tb_lugar_carga_ci::class), version =   1)
abstract class db_lugar_carga_ci : RoomDatabase() {
    abstract fun lugar_carga_ci(): dao_lugar_carga_ci
}
