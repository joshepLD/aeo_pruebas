package com.example.aeo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.activities.DescripcionCartaDos
import com.example.aeo.model.models.ResultadosRemolques
import kotlinx.android.synthetic.main.item_remolques.view.*

class RemolquesAdapter(val contexto: DescripcionCartaDos, val items: ArrayList<ResultadosRemolques>): RecyclerView.Adapter< RemolquesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_remolques, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.placa.text = modelData.placa
        holder.economico.text = modelData.economico
    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
        val placa = view.tvRemolquePlaca
        val economico = view.tvRemolqueEconomico
    }

}