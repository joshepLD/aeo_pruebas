package com.example.aeo.adapter

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.activities.EvidenciasActivity
import com.example.aeo.activities.GaleriaActivity
import com.example.aeo.entity.tb_evidencia_incidencia_ci


class IncidenciaAdapterDos(val contexto: EvidenciasActivity,
                           private var items:List<tb_evidencia_incidencia_ci>

                           ) :RecyclerView.Adapter<IncidenciaAdapterDos.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_evidencias_incidencia, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        //holder.bind(item, deleteIncidencia)
    }


    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {
        private val tv_incidencia = view.findViewById<TextView>(R.id.tvNombreEvidenciaI)
        private val iv_imagen = view.findViewById<ImageView>(R.id.ivImagenEvidenciaI)

        fun bind(
            incidencia: tb_evidencia_incidencia_ci,
            deleteIncidencia: (tb_evidencia_incidencia_ci) -> Unit
        ) {
            tv_incidencia.text = incidencia.nombre
            iv_imagen.setOnClickListener {

            }
            /*iv_imagen.setOnClickListener {
                deleteIncidencia(incidencia)
            }*/

        }
    }

}