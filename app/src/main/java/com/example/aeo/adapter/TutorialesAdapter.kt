package com.example.aeo.adapter

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.model.models.DataTutoriales
import kotlinx.android.synthetic.main.item_tutoriales.view.*


class TutorialesAdapter(val contexto: MainActivity, val items: ArrayList<DataTutoriales>): RecyclerView.Adapter<TutorialesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_tutoriales, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.titulo.text = modelData.titulo
        holder.decripccion.text = modelData.descripcion

        if (modelData.tipoT == "Local") {
            Log.e("Tag", "Local")
            val requestManager = contexto.let { it1 -> Glide.with(it1) }
            requestManager.load(R.drawable.pdf_azul).into(holder.imagen)

        }
        if(modelData.tipoT == "URL") {
            Log.e("Tag", "Link")
            val requestManager = contexto.let { it1 -> Glide.with(it1) }
            requestManager.load(R.drawable.link_www).into(holder.imagen)
        }

        holder.cardView.setOnClickListener{
            var uri = Uri.parse(modelData.url)
            Log.e("Tutoriales", ""+uri)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            /*val intent = Intent(contexto, WebView::class.java)
            intent.putExtra("url", ""+modelData.url)*/
            contexto.startActivity(intent)
        }

    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
        val titulo = view.tvTituloT
        val decripccion = view.tvDescripcionT
        val cardView = view.cvTutoriales
        val imagen = view.imagenT
    }
}