package com.example.aeo.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.aeo.R
import com.example.aeo.activities.DescripcionAnticipo
import com.example.aeo.activities.MainActivity
import com.example.aeo.model.models.DataAnticipos
import kotlinx.android.synthetic.main.item_anticipos.view.*
import kotlinx.android.synthetic.main.item_anticipos_dos.view.*
import kotlinx.android.synthetic.main.item_tutoriales.view.cvTutoriales
import kotlinx.android.synthetic.main.item_tutoriales.view.imagenT

class AnticipoAdapter(val contexto: MainActivity, val items: ArrayList<DataAnticipos>): RecyclerView.Adapter<AnticipoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_anticipos_dos, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.importe.text = modelData.importe
        holder.referencia.text = modelData.numero_referencia
        holder.fecha.text = modelData.fecha_ejecucion

        holder.cardView.setOnClickListener {
            val intent = Intent(contexto, DescripcionAnticipo::class.java)
            intent.putExtra("nombre_beneficiario",""+modelData.nombre_beneficiario)
            intent.putExtra("cuenta_clave_beneficiario",""+modelData.cuenta_clave_beneficiario)
            intent.putExtra("rfc_beneficiario",""+modelData.rfc_beneficiario)
            intent.putExtra("importe",""+modelData.importe)
            intent.putExtra("fecha_ejecucion",""+modelData.fecha_ejecucion)
            intent.putExtra("numero_referencia",""+modelData.numero_referencia)
            intent.putExtra("clave_rastreo",""+modelData.clave_rastreo)
            intent.putExtra("ejecuto",""+modelData.ejecuto)
            contexto.startActivity(intent)
        }

        /*if (modelData.tipoT == "URL") {
            Log.e("Tag", "Local")
            val requestManager = Glide.with(contexto)
            val imageUri = get(R.drawable.ic_pdf)
            val requestBuilder = requestManager.load(imageUri)
            requestBuilder.into(holder.imagen)
        }*/
        /*if(modelData.tipoT == "URL") {
            Log.e("Tag", "Link")
            val requestManager = Glide.with(contexto)
            val imageUri = getItemId(R.drawable.ic_search)
            val requestBuilder = requestManager.load(imageUri)
            requestBuilder.into(holder.imagen)
        }*/



    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
        val importe = view.tvImporte
        val referencia = view.tvReferencia
        val fecha = view.tvFecha
        val cardView = view.cvAnticiposD
    }
}