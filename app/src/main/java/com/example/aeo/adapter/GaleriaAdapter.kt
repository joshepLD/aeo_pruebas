package com.example.aeo.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import kotlinx.android.synthetic.main.item_galeria.view.*
import www.tasisoft.com.db.tb_galeria_ci
import java.util.*


class GaleriaAdapter(private var items:List<tb_galeria_ci>,
                     private val context: Context,
                     private val id_evidencia: String
)
    :RecyclerView.Adapter<GaleriaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_galeria, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val modelSurvey = items.get(position)

        Log.e("Adapter id_evidencia", id_evidencia)
        val param = holder.itemView.getLayoutParams()

        /*if(id_evidencia != modelSurvey.id_evidencia){
            val bmp = BitmapFactory.decodeByteArray(modelSurvey.imagen, 0, modelSurvey.imagen!!.size)
            holder.texto.text = modelSurvey.comentario_imagen
            holder.img.setImageBitmap(bmp)
            holder.itemView.setVisibility(GONE);
            param.height = 0;
            param.width = 0;
            holder.layoutItem.visibility = GONE

        }*/

        val decodedBytes = Base64.getDecoder().decode(modelSurvey.imagen)
        //val decodedString = String(decodedBytes)

        val bmp = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        holder.texto.text = modelSurvey.comentario_imagen
        holder.img.setImageBitmap(bmp)

        /*holder.imgEliminar.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(context)

            dialogBuilder.setMessage("Esta seguro de eliminar esta imagen?")
                .setCancelable(false)
                .setPositiveButton("Si", DialogInterface.OnClickListener {
                        dialog, id -> dialog.dismiss()
                })
                .setNegativeButton("No", DialogInterface.OnClickListener {
                        dialog, id -> dialog.cancel()
                })

            val alert = dialogBuilder.create()
            alert.setTitle("Alerta Eliminar Imagen ")
            alert.show()
        }*/
    }


    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {
        val texto = view.texto
        val img = view.imgPost
        var imgEliminar = view.deleteGalery
        var layoutItem = view.linearLayoutItem

    }

}