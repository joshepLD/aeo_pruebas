package com.example.aeo.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.activities.DescripcionCarta
import com.example.aeo.activities.DescripcionCartaDos
import com.example.aeo.activities.MainActivity
import com.example.aeo.activities.MapasActivity
import com.example.aeo.model.models.DataCI
import kotlinx.android.synthetic.main.item_carta_instrucciones.view.*

class CartaInstruccionAdapter(val contexto: MainActivity, val items:List<DataCI>):RecyclerView.Adapter<CartaInstruccionAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_carta_instrucciones, parent,false))
    }

    override fun getItemCount(): Int {
     return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.folio.text = modelData.ci.folio
        holder.tipoServicio.text = modelData.ci.cartaPorte
        holder.cliente.text = modelData.ci.nombre_cliente_carga


        /*for (i in 0 until modelData.evidencias.evidenciasCarga.size){
            val datoNombre = modelData.evidencias.evidenciasCarga[i].nombreC
            try {
                jsonObject.put("nombre", datoNombre)
            }catch (e:Exception){}
        }*/

        holder.descripcion.setOnClickListener {

        }

        holder.mapas.setOnClickListener {
            val intent = Intent(contexto, MapasActivity::class.java)
            contexto.startActivity(intent)
        }

        /*holder.evidencias.setOnClickListener {
            var jsonObject = JSONObject()
            modelData.evidencias.evidenciasCarga.forEach {
                jsonObject.put("nombre", it.nombreC)
                Log.v("FOREACH", "$jsonObject")
            }
            Log.v("JSON", "$modelData")

            val intent = Intent(contexto, EvidenciasActivity::class.java)

            //intent.putExtra("dataAnswer", modelData.evidencias.evidenciasCarga)
           /* val data = Data.Builder()
                .putString("dataAnswer", jsonAsnwer)
                .build()*/
            contexto.startActivity(intent)

        }*/
    }

    class ViewHolder (view:View):RecyclerView.ViewHolder(view){
        val folio = view.tvFolio
        val tipoServicio = view.tvTipoServicio
        val cliente = view.tvCliente
        val descripcion = view.lyDescripcion
        val mapas = view.lyMapas
        val evidencias = view.lyEvidencias
    }

}