package com.example.aeo.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.aeo.R
import com.example.aeo.activities.EvidenciasActivity
import com.example.aeo.activities.GaleriaActivity
import com.example.aeo.activities.GaleriaActivity2
import com.example.aeo.activities.WebView
import com.example.aeo.model.models.EvidenciasCarga
import com.example.aeo.model.models.EvidenciasDescarga
import kotlinx.android.synthetic.main.item_evidencias_carga.view.*
import kotlinx.android.synthetic.main.item_evidencias_descarga.view.*

class EvidenciasDescargaAdapter(val contexto: EvidenciasActivity, val items: List<EvidenciasDescarga>): RecyclerView.Adapter<EvidenciasDescargaAdapter.ViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_evidencias_descarga, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.nombre.text = modelData.nombreD

        holder.ly_visualizar.setOnClickListener {
            val intent = Intent(contexto, WebView::class.java)
            intent.putExtra("foto_ejemplo",""+modelData.ejemplo)
            contexto.startActivity(intent)
        }

        holder.imagen.setOnClickListener {
            val intent = Intent(contexto, GaleriaActivity2::class.java)
            intent.putExtra("id_evidencia",""+modelData.idEvidencias)
            intent.putExtra("tipo_evidencia","Descarga")
            contexto.startActivity(intent)
        }
       /*val requestManager = Glide.with(contexto)
        val imageUri = modelData.ejemplo
        val requestBuilder = requestManager.load(imageUri)
        requestBuilder.into(holder.imagenD)

        holder.imagenD.setOnClickListener {
            val intent = Intent(contexto, GaleriaActivity::class.java)
            intent.putExtra("id_evidencia",""+modelData.idEvidencias)
            contexto.startActivity(intent)
        }*/
    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
        val nombre = view.tvNombreEvidenciaD
        val imagen = view.ivImagenEvidenciaD
        val ly_visualizar = view.lyVisualizarEjemploD

    }

}