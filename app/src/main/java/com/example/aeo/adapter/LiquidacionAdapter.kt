package com.example.aeo.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.activities.MainActivity
import com.example.aeo.model.models.DataLiquidacion
import kotlinx.android.synthetic.main.item_anticipos.view.*
import kotlinx.android.synthetic.main.item_anticipos.view.lyDescripcion
import kotlinx.android.synthetic.main.item_carta_instrucciones.view.*
import kotlinx.android.synthetic.main.item_liquidacion.view.*
import kotlinx.android.synthetic.main.item_tutoriales.view.*


class LiquidacionAdapter (val contexto: MainActivity, val items: ArrayList<DataLiquidacion>): RecyclerView.Adapter<LiquidacionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        return ViewHolder(LayoutInflater.from(contexto).inflate(R.layout.item_liquidacion, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val modelData = items.get(position)

        holder.liquidacion.text = modelData.fLiquidacion

        holder.cardView.setOnClickListener {
            var uri = Uri.parse(modelData.file)
            val intent = Intent(Intent.ACTION_VIEW, uri)

            contexto.startActivity(intent)
        }
    }

    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
        val liquidacion = view.tvLiquidacionF
        val cardView = view.cvLiquidaciones
    }
}