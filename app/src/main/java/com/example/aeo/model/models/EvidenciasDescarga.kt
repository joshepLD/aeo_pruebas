package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class EvidenciasDescarga (@SerializedName("nombre") var nombreD: String,
                          @SerializedName("ejemplo") var ejemplo: String,
                          @SerializedName("id_evidencia") var idEvidencias: String
)
