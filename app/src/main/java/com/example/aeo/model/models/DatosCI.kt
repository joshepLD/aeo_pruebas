package com.example.aeomovil.model.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DatosCI (@SerializedName("fecha_registro") var fechaRegistro: String,
               @SerializedName("folio_viaje") var folioViaje: String,
               @SerializedName("estatus_carta") var estatusCarta: String)


