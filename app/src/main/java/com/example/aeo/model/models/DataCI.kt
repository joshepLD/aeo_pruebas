package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName


class DataCI (@SerializedName("type") var type: String,
              @SerializedName("ci") var ci: CI
)