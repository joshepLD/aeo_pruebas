package com.example.aeo.model.response

import com.example.aeo.model.models.DataAnticipos
import com.google.gson.annotations.SerializedName

class LeidoResponse (@SerializedName("type_proccess") val type_proccess: String,
                     @SerializedName("data") val dataL: String
)