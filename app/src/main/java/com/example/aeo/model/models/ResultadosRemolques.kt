package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class ResultadosRemolques (@SerializedName("placa")var placa: String,
                           @SerializedName("economico")var economico: String,
                           @SerializedName("area")var area: String
)