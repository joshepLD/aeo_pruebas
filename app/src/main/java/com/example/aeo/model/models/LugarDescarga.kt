package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class LugarDescarga (@SerializedName("datos") var datos: String,
                     @SerializedName("nombre") var nombre: String,
                     @SerializedName("direccion") var direccion: String,
                     @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasLugarDescarga>
)
