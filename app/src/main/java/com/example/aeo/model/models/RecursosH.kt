package com.example.aeomovil.model.models

import com.google.gson.annotations.SerializedName

class RecursosH (@SerializedName("incapacidad" ) var incapacidad: String,
                 @SerializedName("vacaciones" ) var vacaciones: String,
                 @SerializedName("datos") var datos: Datos)

/*
* {
    "Solicitud_turno": "true",
    "resultado": "No hay registros de solicitud",
    "RH": {
        "incapacidad": "true",
        "vacaciones": "true",
        "datos": {
            "evento": null,
            "fecha_inicio": null,
            "fecha_termino": null
        }
    },
    "Carta_instrucciones": "true"
}
* */