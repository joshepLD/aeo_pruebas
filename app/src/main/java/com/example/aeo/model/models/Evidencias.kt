package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Evidencias (@SerializedName("datos") var datos: String,
                  @SerializedName("carga") var evidenciasCarga: ArrayList<EvidenciasCarga>,
                  @SerializedName("descarga") var evidenciasDescarga: ArrayList<EvidenciasDescarga>
)