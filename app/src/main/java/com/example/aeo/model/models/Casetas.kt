package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Casetas (@SerializedName("datos") var datos_casetas: String,
               @SerializedName("resultado") var resultado_casetas: ArrayList<ResultadoCasetas>
)