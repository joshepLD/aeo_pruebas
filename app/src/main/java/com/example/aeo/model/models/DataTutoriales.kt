package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class DataTutoriales (@SerializedName("title") var titulo : String,
                      @SerializedName("description") var descripcion : String,
                      @SerializedName("type_tutorial") var tipoT : String,
                      @SerializedName("url") var url : String
)