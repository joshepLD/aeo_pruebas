package com.example.aeo.model.response

import com.example.aeo.model.models.DataLiquidacion
import com.google.gson.annotations.SerializedName

class LiquidacionResponse (@SerializedName("travel_liquidation") var Liquidacion : String,
                           @SerializedName("data_travel_liquidation") var dataLiquidacion : ArrayList<DataLiquidacion>

)