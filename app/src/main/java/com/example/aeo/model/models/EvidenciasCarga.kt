package com.example.aeo.model.models

import com.example.aeo.activities.EvidenciasActivity
import com.google.gson.annotations.SerializedName
import org.jetbrains.anko.AnkoAsyncContext

class EvidenciasCarga(@SerializedName("nombre") var nombreC: String,
                      @SerializedName("ejemplo") var ejemplo: String,
                      @SerializedName("id_evidencia") var idEvidencias: String
)


