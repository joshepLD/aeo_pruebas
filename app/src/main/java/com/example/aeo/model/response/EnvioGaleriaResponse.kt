package com.example.aeo.model.response

import com.google.gson.annotations.SerializedName

class EnvioGaleriaResponse (@SerializedName("type_proccess") val typeProccess: String,
                            @SerializedName("data") val data: String
)