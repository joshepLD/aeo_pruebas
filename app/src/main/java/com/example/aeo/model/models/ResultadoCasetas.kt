package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class ResultadoCasetas (@SerializedName("nombre") var nombre_caseta: String,
                        @SerializedName("alias") var alias_caseta: String
)