package com.example.aeomovil.model.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Login (@SerializedName("id_usuario") var idUser: String,
             @SerializedName("id_operador") var idOperador: String,
             @SerializedName("usuario") var usuario: String,
             @SerializedName("contasena") var password: String,
             @SerializedName("nombre_usuario") var name: String,
             @SerializedName("apellido_paterno") var apellidoP: String,
             @SerializedName("apellido_materno") var apellidoM: String,
             @SerializedName("tipo Acceso") var tipoAcceso: String,
             @SerializedName("operacion_usuario") var operacionU: String,
             @SerializedName("clave_operacion") var claveOperacion: String,
             @SerializedName("ruta_imagen") var rutaImagen: String,
             @SerializedName("fecha_imagen") var fechaImagen: String

)

//"ruta_imagen": "/image/photo_user/1587149587epn_80.jpg",
//"fecha_imagen": "2020-04-17 13:53:07"