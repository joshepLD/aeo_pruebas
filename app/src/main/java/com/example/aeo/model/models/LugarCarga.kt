package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class LugarCarga (@SerializedName("datos") var datos: String,
                  @SerializedName("nombre") var nombre: String,
                  @SerializedName("direccion") var direccion: String,
                  @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasLugarCarga>
)