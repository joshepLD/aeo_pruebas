package com.example.aeomovil.model.response

import com.example.aeo.model.models.DataCI
import com.google.gson.annotations.SerializedName

class CartaInstruccionesResponse (@SerializedName("type_proccess") var TypeProccess : String,
                                  @SerializedName("carta_instrucciones") var CartaInstrucciones : String,
                                  @SerializedName("data") var dataCI: DataCI)


