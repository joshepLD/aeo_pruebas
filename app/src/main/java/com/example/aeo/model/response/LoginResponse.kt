package com.example.aeomovil.model.response

import com.example.aeomovil.model.models.Login
import com.example.aeomovil.model.models.Smartphone
import com.google.gson.annotations.SerializedName

class LoginResponse (@SerializedName("estatus") var estatus: String,
                     @SerializedName("resultado") var resultado: Login,
                     @SerializedName("smartphone") var smartphone: Smartphone)