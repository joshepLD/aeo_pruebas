package com.example.aeo.model.response

import com.example.aeo.model.models.DataAnticipos
import com.google.gson.annotations.SerializedName

class AnticiposResponse (@SerializedName("advance_payment") val advancePayment: String,
                         @SerializedName("data_advance_payment") val data: ArrayList<DataAnticipos>
)