package com.example.aeo.model.response

import com.google.gson.annotations.SerializedName

class VerificarTurnoResponse (@SerializedName("shift_request") var request: String,
                              @SerializedName("date_request") var fecha: String
                              )