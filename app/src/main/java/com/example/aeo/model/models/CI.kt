package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName


class CI ( @SerializedName("id_carta") var idCarta: String,
           @SerializedName("folio") var folio: String,
           @SerializedName("operacion") var operacion: String,
           @SerializedName("operacion_abrv") var operacion_abrv: String,
           @SerializedName("tipo_servicio")var tipoServicio: String,
           @SerializedName("tipo_viaje")var tipoViaje: String,
           @SerializedName("placa")var placa: String,
           @SerializedName("eco_camion")var eco_camion: String,
           @SerializedName("cliente")var nombre_cliente_carga: String,
           @SerializedName("cliente_paga")var clientePaga: String,
           @SerializedName("numero_viaje")var numeroViaje: String,
           @SerializedName("carta_porte")var cartaPorte: String,
           @SerializedName("nombre_contacto_carga")var nombreContactoCarga: String,
           @SerializedName("tel_contacto_carga")var telContactoCarga: String,
           @SerializedName("horario_inicio")var horarioInicio: String,
           @SerializedName("horario_carga")var horarioCarga: String,
           @SerializedName("horario_descarga")var horarioDescarga: String,
           @SerializedName("maniobras")var maniobras: String,
           @SerializedName("observaciones")var observaciones: String,
           @SerializedName("tipo_inicio")var tipoInicio: String,
           @SerializedName("evidencias_observaciones")var evidenciasObservaciones: String,
           @SerializedName("vista_gral")var vista_general: String,
           @SerializedName("vista_detallada")var vista_detallada: String,
           @SerializedName("inicio")var inicio: Inicio,
           @SerializedName("semirremolques")var remolques: Remolques,
           @SerializedName("lugar_carga")var lugarCarga: LugarCarga,
           @SerializedName("lugar_descarga")var lugarDescarga: LugarDescarga,
           @SerializedName("ruta")var ruta: Ruta,
           @SerializedName("evidencias")var evidencias: Evidencias
)