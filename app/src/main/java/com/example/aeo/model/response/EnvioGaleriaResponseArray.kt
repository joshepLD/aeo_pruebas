package com.example.aeo.model.response

import com.example.aeo.model.models.DataTutoriales
import com.google.gson.annotations.SerializedName

class EnvioGaleriaResponseArray (@SerializedName("type_proccess") val typeProccess: String,
                                 @SerializedName("data") val data: ArrayList<String>
)