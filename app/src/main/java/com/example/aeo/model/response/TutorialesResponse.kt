package com.example.aeo.model.response

import com.example.aeo.model.models.DataTutoriales
import com.google.gson.annotations.SerializedName

class TutorialesResponse (@SerializedName("number_tutorials") var numeroT : Int,
                          @SerializedName("data") var dataT : ArrayList<DataTutoriales>)