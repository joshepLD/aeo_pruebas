package com.example.aeo.model.response

import com.google.gson.annotations.SerializedName

class EnvioGaleriaResponseB (@SerializedName("type_proccess") val typeProccess: String,
                             @SerializedName("data") val data: String
)