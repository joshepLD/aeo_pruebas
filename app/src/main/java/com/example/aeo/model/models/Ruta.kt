package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Ruta (@SerializedName("datos") var datos: String,
            @SerializedName("nombre") var nombre: String,
            @SerializedName("alias") var alias: String,
            @SerializedName("casetas") var casetas: Casetas,
            @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasRuta>
)