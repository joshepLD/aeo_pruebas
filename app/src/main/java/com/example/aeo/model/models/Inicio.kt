package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class Inicio (@SerializedName("datos") var datos: String,
              @SerializedName("nombre") var nombre: String,
              @SerializedName("direccion") var direccion: String,
              @SerializedName("tipo") var tipo: String,
              @SerializedName("coordenadas") var coordenadas: ArrayList<CoordenadasInicio>
)
