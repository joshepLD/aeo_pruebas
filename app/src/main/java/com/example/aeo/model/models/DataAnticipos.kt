package com.example.aeo.model.models

import com.google.gson.annotations.SerializedName

class DataAnticipos (@SerializedName("nombre_beneficiario") var nombre_beneficiario: String,
                     @SerializedName("cuenta_clave_beneficiario") var cuenta_clave_beneficiario: String,
                     @SerializedName("rfc_beneficiario") var rfc_beneficiario: String,
                     @SerializedName("importe") var importe: String,
                     @SerializedName("fecha_aplicacion") var fecha_aplicacion: String,
                     @SerializedName("numero_referencia") var numero_referencia: String,
                     @SerializedName("clave_rastreo") var clave_rastreo: String,
                     @SerializedName("ejecuto") var ejecuto: String,
                     @SerializedName("fecha_ejecucion") var fecha_ejecucion: String
                     )
