package com.example.aeo.WorkManager

import com.example.aeo.model.response.EnvioGaleriaResponseArray
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.POST
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

class APIClient {

    private val API_BASE_URL = "http://189.210.119.125:18080/web_services/"
    private lateinit var servicesApiInterface:ServicesApiInterface

    fun build():ServicesApiInterface{
        var builder: Retrofit.Builder = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())

        var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor())

        var retrofit: Retrofit = builder.client(httpClient.build()).build()
        servicesApiInterface = retrofit.create(
                ServicesApiInterface::class.java)

        return servicesApiInterface
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level=HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    interface ServicesApiInterface {
        @FormUrlEncoded
        @POST("WSUploadFileTypeArray.php")
        fun envioGaleria(@Field("dataGalery") arrayImages: String) : Call<EnvioGaleriaResponseArray>

    }
}