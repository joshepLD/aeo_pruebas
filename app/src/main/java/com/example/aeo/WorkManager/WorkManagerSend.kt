package com.example.aeo.WorkManager

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson

class WorkManagerSend (private val mContext: Context, workerParams: WorkerParameters) :
    Worker(mContext, workerParams) {


    override fun doWork():Result {
        return  try {
            val data = inputData.getString("dataAnswer")
            var modelAnswerSend = data.toString()
            Log.v("JSONPARAMWORKMANAGER", "* $modelAnswerSend")
            val callResponse = APIClient().build().envioGaleria(modelAnswerSend).execute()
            if (callResponse.isSuccessful) {
                Log.v("WorManager", "Ok___")

                Result.success()
            } else {
                Log.v("WorManager", "Error___")

                Result.failure()
            }
        }catch (e:Exception){
            Result.failure()
        }
    }

}