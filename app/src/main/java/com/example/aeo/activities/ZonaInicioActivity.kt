package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.SeekBar
import com.example.aeo.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.PolygonOptions

class ZonaInicioActivity :
    AppCompatActivity(),
    OnMapReadyCallback,
    SeekBar.OnSeekBarChangeListener,
    AdapterView.OnItemSelectedListener{

    private lateinit var mutablePolygon: Polygon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zona_inicio)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        // return early if the map was not initialised properly
        googleMap ?: return

        with(googleMap) {
            // Override the default content description on the view, for accessibility mode.
            setContentDescription(getString(R.string.polygon_demo_description))
            // Move the googleMap so that it is centered on the mutable polygon.
            moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(19.433470930118744, -99.13407555389406), 20f))

            // Create a rectangle with two rectangular holes.
            mutablePolygon = addPolygon(PolygonOptions().apply {
                add(
                    LatLng(19.433470930118744, -99.13407555389406),
                    LatLng(19.432590696733175, -99.13228383827212),
                    LatLng(19.432165754770534, -99.13416138458254)

                )
                //addHole(createRectangle(center2, .001, .001))
                //addHole(createRectangle(center3, .001, .001))
                fillColor(2130771967)
                strokeColor(-65536)
                //strokeWidth(strokeWidthBar.progress.toFloat())
                //clickable(clickabilityCheckbox.isChecked)
            })

            mutablePolygon.setTag("Metro Chilpancingo");

            // Add a listener for polygon clicks that changes the clicked polygon's stroke color.
            setOnPolygonClickListener { polygon ->
                // Flip the red, green and blue components of the polygon's stroke color.
                polygon.strokeColor = polygon.strokeColor xor 0x00ffffff
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        // do nothing
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        // do nothing
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        // do nothing
    }

    /**
     * Listener for when an item is selected using a spinner.
     * Can change line pattern and joint type.
     */
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // don't do anything here
    }
}
