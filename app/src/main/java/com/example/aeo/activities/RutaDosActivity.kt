package com.example.aeo.activities

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.CheckBox
import android.widget.SeekBar
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.example.aeo.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class RutaDosActivity :
    AppCompatActivity(),
    OnMapReadyCallback,
    SeekBar.OnSeekBarChangeListener,
    AdapterView.OnItemSelectedListener {

    private val CUSTOM_CAP_IMAGE_REF_WIDTH_PX = 50
    private val INITIAL_STROKE_WIDTH_PX = 10
    private val MAX_WIDTH_PX = 100
    private val MAX_HUE_DEGREES = 360
    private val MAX_ALPHA = 255
    private val PATTERN_DASH_LENGTH_PX = 50
    private val PATTERN_GAP_LENGTH_PX = 20


    // City locations for mutable polyline. Linea Roja
    private val adelaideLatLng = LatLng(19.405905726458414, -99.16854998636248)
    private val darwinLatLng = LatLng(19.405895607144306, -99.16882893610003)
    private val melbourneLatLng = LatLng(19.405895607144306, -99.16905424165728)
    private val perthLatLng = LatLng(19.405890547486976, -99.169365377903)
    private val cinco = LatLng(19.405890547486976, -99.16981598901751)
    private val seis = LatLng(19.405870308856052, -99.17074403333666)
    private val siete = LatLng(19.40581465260803, -99.1719939427376)
    private val ocho = LatLng(19.405764056002425, -99.17307755517962)
    private val nueve = LatLng(19.405758996341, -99.17436501550677)
    private val diez = LatLng(19.405728638369133, -99.17600116300585)
    private val once = LatLng(19.405622385422966, -99.17720815706255)
    private val doce = LatLng(19.40600691956591, -99.17830786275866)
    private val trece = LatLng(19.40617388805543, -99.178908677578)

    /*private val dot = Dot()
    private val dash = Dash(PATTERN_DASH_LENGTH_PX.toFloat())
    private val gap = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
    private val patternDotted = Arrays.asList(dot, gap)
    private val patternDashed = Arrays.asList(dash, gap)
    private val patternMixed = Arrays.asList(dot, gap, dot, dash, gap)*/


    //private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ruta_dos)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun getResourceStrings(resourceIds: IntArray): List<String> {
        return resourceIds.map { getString(it) }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        googleMap?:return
        with(googleMap) {
            // Override the default content description on the view, for accessibility mode.
            setContentDescription(getString(R.string.polyline_demo_description))
            // A geodesic polyline that goes around the world.
            addPolyline(PolylineOptions().apply {
                add( LatLng(19.405905726458414, -99.16854998636248), darwinLatLng, melbourneLatLng, perthLatLng, cinco, seis, siete, ocho, nueve, diez, once, doce)
                width(INITIAL_STROKE_WIDTH_PX.toFloat())
                color(Color.RED)
                geodesic(true)
                //clickable(clickabilityCheckbox.isChecked)
            })

            /*val call =  respuesta.lista[0]lcordenada
                    for (i in 0 until  call.size){
                        val data = call[i].doble
                        val caordenas = data
                        Cordenas.insert(caordenas)
                    }

                    id 12



            var listasTotalCordenas tipodoble
            doAsync {
                //consulta
                val consulta = Cordenas.getCordenas
                uiThread {
                    for (i in 0 until  consulta.seze){
                        val data = consulta[i]
                        listasTotalCordenas.add(data)

                    }
                }
            }*/

            // Move the googleMap so that it is centered on the mutable polyline.
            moveCamera(CameraUpdateFactory.newLatLngZoom(siete, 16f))

            // Add a listener for polyline clicks that changes the clicked polyline's color.
            setOnPolylineClickListener { polyline ->
                // Flip the values of the red, green and blue components of the polyline's color.
                polyline.color = polyline.color xor 0x00ffffff
            }
        }

        // A simple polyline across Australia. This polyline will be mutable.
        /*mutablePolyline = googleMap.addPolyline(PolylineOptions().apply{
            color(Color.RED)
            width(INITIAL_STROKE_WIDTH_PX.toFloat())
            clickable(true)
            add(LatLng(19.405906, -99.168550),
                LatLng(19.405896, -99.168829),
                LatLng(19.405896, -99.169054)
            )
        })*/
    }
    //
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        // Don't do anything here.
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        // Don't do anything here.
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        // Don't do anything here.
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // Don't do anything here.
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // Don't do anything here.
    }
}