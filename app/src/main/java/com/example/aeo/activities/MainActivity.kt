package com.example.aeo.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.example.aeo.R
import com.example.aeo.fragments.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_solicitud_turno.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var toolbar: Toolbar
    lateinit var toolbarText: TextView
    lateinit var textNameMenu: TextView
    lateinit var drawerLayout: DrawerLayout
    lateinit var navViewAdministracion: NavigationView

    private lateinit var nombre: String
    private var banderaBack: Boolean = true
    private lateinit var foto: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // var userName= findViewById<TextView>(R.id.nombreUser)

        val sharedPreference = getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        nombre = sharedPreference.getString("usuario", "").toString()
        foto = sharedPreference.getString("ruta_imagen", "").toString()


        Log.e("Tag", "nombre--->" + nombre)
        Log.e("Tag", "foto--->" + foto)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navViewAdministracion = findViewById(R.id.nav_administrador)
        navViewAdministracion.setNavigationItemSelectedListener(this)

        if (null == savedInstanceState) {
            val blankFragment = HomeFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, blankFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        }

        Log.e("Tag", "onCreate")
        userName(nombre, foto)
    }

    private fun userName(nameUser: String, fotoUser: String) {
        Log.v("NOMBREUSEUARIO ", "$nameUser")
        var header = navViewAdministracion.getHeaderView(0)
        var userName = header.findViewById<TextView>(R.id.nombreUserHeader)
        var userImagen = header.findViewById<ImageView>(R.id.cvPerfil)
        userName.setText(nameUser)

        if (fotoUser == "noRute"){
            val requestManager = this.let { it1 -> Glide.with(it1) }
            requestManager.load(R.drawable.ic_user).into(userImagen)
        }else{
            val requestManager = Glide.with(this)
            val requestBuilder = requestManager.
            load(R.drawable.ic_user)
            requestBuilder.into(userImagen)
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        Log.e("Tag", "onNavigation")
        when (item.itemId) {
            R.id.nav_inicio -> {

                tVnameToolbar.text = getString(R.string.home)
                val homeFragment = HomeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, homeFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                banderaBack = true
            }
            R.id.solicitud_turno -> {
                Log.e("Tag", "item Ayuda")
                tVnameToolbar.text = getString(R.string.solicitar_turno)
                val solicitudTurnoFragment = SolicitudTurnoFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, solicitudTurnoFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                banderaBack = false
            }
            R.id.nav_ci -> {
                tVnameToolbar.text = getString(R.string.ci)
                val cartaInstruccionesFragment = CartaInstruccionesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, cartaInstruccionesFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                banderaBack = false
            }
            R.id.nav_anticipo -> {
                tVnameToolbar.text = "Anticipo (s) Banco"
                val anticipoFragment = AnticipoFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, anticipoFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
                banderaBack = false
            }
            /*R.id.nav_liquidacion -> {
                tVnameToolbar.text = "Liquidacion"
                val liquidacionFragment = LiquidacionFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, liquidacionFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
            }*/
            /*R.id.nav_tutoriales -> {
                tVnameToolbar.text = getString(R.string.tutoriales)
                val tutorialesFragment = TutorialesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, tutorialesFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                supportFragmentManager.executePendingTransactions()
            }*/
            /*R.id.nav_sesion -> {
                onBackPressed()
            }*/
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        if (banderaBack == false){
            tVnameToolbar.text = "Inicio"
            val homeFragment = HomeFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, homeFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
            supportFragmentManager.executePendingTransactions()
            banderaBack = true
        }else{
            super.onBackPressed()
        }


        /*val alerDialog = AlertDialog.Builder(this)
        alerDialog.setTitle("Alerta")
        alerDialog.setMessage("Cerrar sesión?")
        alerDialog.setPositiveButton("Si"){dialog, which ->
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        alerDialog.setNegativeButton("No"){dialog, which ->
        }
        val dialog = alerDialog.create()
        dialog.show()*/

    }

    /*private fun BottomNavigationView.deselectAllItems() {
        val menu = this.menu
        for(i in 0 until menu.size()) {
            (menu.getItem(i) as? MenuItemImpl)?.let {
                it.isExclusiveCheckable = false
                it.isChecked = false
                it.isExclusiveCheckable = true
            }
        }
    }*/
}
