package com.example.aeo.activities

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.aeo.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class ZonaCargaActivity :
    AppCompatActivity(),
    OnMapReadyCallback,
    SeekBar.OnSeekBarChangeListener,
    AdapterView.OnItemSelectedListener {

    private lateinit var mutablePolygon: Polygon

    private lateinit var id_carta_carga: String
    private lateinit var dato_carga: String
    private lateinit var nombre_carga: String
    private lateinit var direccion_carga: String


    // These are the options for polygon stroke joints and patterns. We use their
    // string resource IDs as identifiers.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zona_carga)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        /*doAsync {
            Log.v("Consulta","")
            val consualta = SCPMobileApp.tablaLugarCargaCi.lugar_carga_ci().getAllLugarCarga()
            for (i in 0 until consualta.size){
                id_carta_carga = consualta[i].id_carta
                dato_carga = consualta[i].datos
                nombre_carga = consualta[i].nombre
                direccion_carga = consualta[i].direccion
                uiThread {
                    Log.e("CLIENTE","$id_carta_carga")
                }
            }
        }*/
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        // return early if the map was not initialised properly
        googleMap ?: return

        with(googleMap) {
            // Override the default content description on the view, for accessibility mode.
            setContentDescription(getString(R.string.polygon_demo_description))
            // Move the googleMap so that it is centered on the mutable polygon.
            moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(19.405989, -99.168437), 20f))

            // Create a rectangle with two rectangular holes.
            mutablePolygon = addPolygon(PolygonOptions().apply {
                add(
                    LatLng(19.405957, -99.168606),
                    LatLng(19.405989, -99.168437),
                    LatLng(19.405761, -99.168507),
                    LatLng(19.405806, -99.168659)
                    )
                //addHole(createRectangle(center2, .001, .001))
                //addHole(createRectangle(center3, .001, .001))
                fillColor(2130771967)
                strokeColor(-65536)
                //strokeWidth(strokeWidthBar.progress.toFloat())
                //clickable(clickabilityCheckbox.isChecked)
            })

            mutablePolygon.setTag("Metro Chilpancingo");

            // Add a listener for polygon clicks that changes the clicked polygon's stroke color.
            setOnPolygonClickListener { polygon ->
                // Flip the red, green and blue components of the polygon's stroke color.
                polygon.strokeColor = polygon.strokeColor xor 0x00ffffff
            }
        }

    }


    fun toggleClickability(view: View) {
        if (view is CheckBox) {
            mutablePolygon.isClickable = view.isChecked
        }
    }


    /**
     * Listener that is called when a seek bar is moved.
     * Can change polygon fill color/transparency, stroke color/transparency and stroke width.
     */
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        // do nothing
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        // do nothing
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        // do nothing
    }

    /**
     * Listener for when an item is selected using a spinner.
     * Can change line pattern and joint type.
     */
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // don't do anything here
    }

}
