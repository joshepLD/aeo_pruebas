package com.example.aeo.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.adapter.EvidenciasCargaAdapter
import com.example.aeo.adapter.EvidenciasDescargaAdapter
import com.example.aeo.adapter.IncidenciaAdapter
import com.example.aeo.entity.tb_evidencia_incidencia_ci
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.activity_evidencias.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


class EvidenciasActivity : AppCompatActivity() {
    private lateinit var idOperador: String
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayout:LinearLayoutManager
    private lateinit var recyclerViewD: RecyclerView
    private lateinit var linearLayoutD:LinearLayoutManager

    lateinit var incidencias: MutableList<tb_evidencia_incidencia_ci>
    lateinit var adapterIncidencia: IncidenciaAdapter
    private lateinit var recyclerViewI: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evidencias)
        recyclerView = findViewById(R.id.rvEvidenciasCarga)
        recyclerViewD = findViewById(R.id.rvEvidenciasDescarga)

        incidencias = ArrayList()
        getIncidencias()

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Evidencias"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val  sharedPreference = this.getSharedPreferences(
            "mi_app_aeo",
            Context.MODE_PRIVATE
        )
        idOperador = "4"//sharedPreference?.getString("id_operador","").toString()

        switchIncidencias.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                // The switch is enabled/checked
                Toast.makeText(this, "Switch ON", Toast.LENGTH_LONG).show()
                lyIncidencias.visibility = VISIBLE
                lyAgregarIncidencia.visibility = VISIBLE
                contentIncidencias.visibility = VISIBLE
            } else {
                // The switch is disabled
                Toast.makeText(this, "Switch OFF", Toast.LENGTH_LONG).show()
                lyIncidencias.visibility = GONE
                lyAgregarIncidencia.visibility = GONE
                contentIncidencias.visibility = GONE
            }
        }
        agregarIncidencia.setOnClickListener {
            addIncidencia(tb_evidencia_incidencia_ci(nombre = edtAgregarIncidencia.text.toString()))
            Log.e("Tag", "Paso Uno")
        }
      /*val nombre: String = intent.getStringExtra("nombre")
        val ejemplo: String = intent.getStringExtra("ejemplo")
        val idEvidencia: String = intent.getStringExtra("id_evidencia")
        val Evidencia: String = intent.getStringExtra("dataAnswer")*/
        searchCI(idOperador)
    }

    private fun searchCI(id: String){
        Log.e("Tag", "Hola search CI")
        doAsync {
            val call = getRetrofit().create(APIService::class.java).cartaInstrucciones(id).execute()
            val response = call.body() as CartaInstruccionesResponse
            uiThread {
                try {
                    linearLayout = LinearLayoutManager(this@EvidenciasActivity)
                    recyclerView.layoutManager = linearLayout
                    recyclerView.adapter = EvidenciasCargaAdapter(this@EvidenciasActivity, response.dataCI.ci.evidencias.evidenciasCarga)

                    linearLayoutD = LinearLayoutManager(this@EvidenciasActivity)
                    recyclerViewD.layoutManager = linearLayoutD
                    recyclerViewD.adapter = EvidenciasDescargaAdapter(this@EvidenciasActivity, response.dataCI.ci.evidencias.evidenciasDescarga)

                }catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getIncidencias() {
        doAsync {
            incidencias = SCPMobileApp.tablaIncidenciaCi.incidenciaDao().getAllIncidencias()
            Log.e("Tag", "getIncidencias")
            uiThread {
                setUpRecyclerView(incidencias)

            }
        }
    }

    fun addIncidencia(incidencia: tb_evidencia_incidencia_ci){
        Log.e("Tag","paso 2")
        doAsync {
            Log.e("Tag","paso 3")
            val id = SCPMobileApp.tablaIncidenciaCi.incidenciaDao().addIncidencia(incidencia)
            //val recovery = SCPMobileApp.tablaIncidenciaCi.incidenciaDao().getTaskById(id)
            uiThread {

                Log.e("Tag","paso 6")
                incidencias.add(incidencia)
                adapterIncidencia.notifyItemInserted(incidencias.size)
                Toast.makeText(this@EvidenciasActivity, "Incidencia Agregada", Toast.LENGTH_LONG).show()
                getIncidencias()
                clearFocus()
                hideKeyboard()
            }
        }
    }

    fun updateIncidencia(incidencia: tb_evidencia_incidencia_ci) {
        doAsync {
            SCPMobileApp.tablaIncidenciaCi.incidenciaDao().updateIncidencia(incidencia)
        }
    }

    fun clearFocus(){
        edtAgregarIncidencia.setText("")
    }

    fun Context.hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    fun setUpRecyclerView(incidencias: List<tb_evidencia_incidencia_ci>) {
        Log.e("Tag", "setUpRecyclerView")
        adapterIncidencia = IncidenciaAdapter(this@EvidenciasActivity, incidencias, { updateIncidencia(it) })
        recyclerViewI = findViewById(R.id.rvIncidencias)
        recyclerViewI.setHasFixedSize(true)
        recyclerViewI.layoutManager = LinearLayoutManager(this)
        recyclerViewI.adapter = adapterIncidencia
        Log.e("DB","Datos recuperados")
        Log.e("DB", adapterIncidencia.toString())
    }

    private fun toasMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
