package com.example.aeo.activities

class EjemploInsert {
    /*doAsync {
        Log.d("TAG", "fechaEnvioSurvey "+fechaDefaulSurvey)
        val call = retrofitBase().create(APIService::class.java).getAllSurveys(fechaDefaulSurvey).execute()
        progreesDialog.dismiss()
        try {
            if (call.code() == 200) {
                val response = call.body() as ResponseSurveys
                var gson = Gson()
                var json = gson.toJson(response)
                var jsonObject = JSONObject(json)
                var objectArray: JSONArray = jsonObject.getJSONArray("Encuestas")
                Log.d("TAG", "jsonSurvey " + objectArray)
                if (objectArray.length()== 0){
                    uiThread {
                        Toast.makeText(activity, "Por el momento no hay información por actualizar", Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    for (i in 0 until objectArray.length()) {
                        var sObjet: JSONObject = objectArray.getJSONObject(i)
                        val listnombreEncuesta = sObjet.get("nombreEncuesta").toString()
                        val listidEncuesta = sObjet.getInt("idEncuesta")
                        val listidUnidadDeNegocioEncuesta = sObjet.getInt("idUnidadDeNegocio")
                        //val idIdomaEncuesta = sObjet.getInt("idIdioma")
                        val listDescripcionEncuesta = sObjet.getString("Descripcion").toString()
                        val listBorradoEncuesta:Boolean= sObjet.getBoolean("borrado")
                        val listMinitoEncuesta = sObjet.getInt("minutos")
                        val lisPreguntas = sObjet.getJSONArray("listaDePreguntas")
                        Log.d("TAG", "forpreguntas " + lisPreguntas.toString())
                        Log.d("TAG", "Datos encuesta \n" +
                                listnombreEncuesta + "\n" +
                                listidEncuesta + "\n" +
                                listidUnidadDeNegocioEncuesta + "\n" +
                                listMinitoEncuesta)

                        //insert db local
                        var dataSurvey = Surveys( encuestas1 , 10 , check)
                        val dataSurvery = Surveys(
                            listidEncuesta,
                            listnombreEncuesta,
                            listidUnidadDeNegocioEncuesta,
                            listDescripcionEncuesta,
                            listBorradoEncuesta,
                            listMinitoEncuesta)
                        Log.d("TAG", "Encuestas " + dataSurvery.toString())
                        Encuesta.databaseEncuestas.encuestasDAO().insertSurvey(dataSurvery)

                        //for par iterar preguntas
                        for (g in 0 until lisPreguntas.length()) {
                            val sObjetPreguntas: JSONObject = lisPreguntas.getJSONObject(g)
                            val listBorradoPregunta:Boolean = sObjetPreguntas.getBoolean("borrado")
                            val listTipoPregunta = sObjetPreguntas.get("tipoPregunta").toString()
                            val listidTipoPregunta = sObjetPreguntas.getInt("idTipoPregunta")
                            val listIdPregunta = sObjetPreguntas.getInt("idPregunta")
                            val listIdIdioma = sObjetPreguntas.getInt("idIdioma")
                            val listordenEncuesta = sObjetPreguntas.getInt("ordenEnEncuesta")
                            val listPseudoID = sObjetPreguntas.get("PseudoID").toString()
                            val listNombrePregunta = sObjetPreguntas.get("nombrePregunta").toString()
                            val listEsRequerido = sObjetPreguntas.getBoolean("esRequerido")
                            val listTieneCampoOtros = sObjetPreguntas.getBoolean("tieneCampoOtros")
                            //val listOpciones= sObjetPreguntas.get("Opciones")
                            val listTextoOtros = sObjetPreguntas.get("textoOtros").toString()
                            val listVisibleSi = sObjetPreguntas.get("visibleSi").toString()
                            val listRequeridoSi = sObjetPreguntas.get("requeridoSi").toString()
                            val listNombreSiEsTrue = sObjetPreguntas.get("nombreSiEsTrue").toString()
                            val listNombreSiEsFalse = sObjetPreguntas.get("nombreSiEsFalse").toString()
                            val listTieneSeleccionarTodos:Boolean = sObjetPreguntas.getBoolean("tieneSeleccionarTodos")
                            val listOpcionesVisiblesSi = sObjetPreguntas.get("opcionesVisiblesSi").toString()
                            val listOpciones = sObjetPreguntas.getJSONArray("Opciones")

                            Log.d("TAG", "DatosPregunta " + listTipoPregunta + " " + listidTipoPregunta + " " + listNombrePregunta + " " + listEsRequerido + " " + listTieneSeleccionarTodos)

                            val preguntasInsert = Questions(
                                listIdPregunta,
                                listBorradoPregunta,
                                listTipoPregunta,
                                listidTipoPregunta,
                                listIdIdioma,
                                listordenEncuesta,
                                listPseudoID,
                                listNombrePregunta,
                                listEsRequerido,
                                listTieneCampoOtros,
                                listTextoOtros,
                                listVisibleSi,
                                listRequeridoSi,
                                listNombreSiEsTrue,
                                listNombreSiEsFalse,
                                listTieneSeleccionarTodos,
                                listOpcionesVisiblesSi,
                                listidEncuesta
                                //idEncuesta
                            )
                            Log.d("TAG", "pregunta " + preguntasInsert.toString())
                            Encuesta.databasePreguntas.preguntasDAO().insertPreguntas(preguntasInsert)

                            Log.d("TAG", "opciones " + listOpciones.toString())
                            for (s in 0 until listOpciones.length()){
                                val sObjetPreguntasOpciones: JSONObject = listOpciones.getJSONObject(s)
                                val listOptionPreguntaValor = sObjetPreguntasOpciones.getString("valor")
                                val listOptionPreguntaTexto = sObjetPreguntasOpciones.getString("texto")
                                var opcionesPreguntas = QuestionsOptions(0,listIdPregunta,listIdIdioma, listOptionPreguntaValor,listOptionPreguntaTexto )
                                Encuesta.databaseQuestionOptions.questionOptionDao().insertQuestionOption(opcionesPreguntas)
                            }
                        }

                    }

                    var idSincronization = 1
                    var sharedPreferencesSincronizate = context?.let { it1 -> SharedPreference(it1) }
                    sharedPreferencesSincronizate?.save("banderaSincronizationSurvey", idSincronization.toString().toInt())
                    uiThread {
                        Toast.makeText(activity, "La sincronización de encuestas fue exitosamente", Toast.LENGTH_LONG
                        ).show()
                        val formater = SimpleDateFormat("yyyy-MM-dd H:mm:ss")
                        var formateDate = formater.format(Date())
                        fechaActual = formateDate
                        insertDateSurvey(fechaActual)
                    }
                }
            } else {
                uiThread {
                    Toast.makeText(activity, "Algo salio mal intente nuevamente! ", Toast.LENGTH_LONG
                    ).show()
                }
            }
        } catch (ex: Exception) {

        }
    }*/
}