package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.aeo.R

class DescripcionAnticipo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_descripcion_anticipo)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Descripción Anticipo"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val tvNombreBeneficiario = findViewById<TextView>(R.id.tvNombreBeneficiarioA)
        val tvRfc = findViewById<TextView>(R.id.tvRfcA)
        val tvClave = findViewById<TextView>(R.id.tvClaveA)
        val tvImporteA = findViewById<TextView>(R.id.tvImporteA)
        val tvFechaA = findViewById<TextView>(R.id.tvFechaA)
        val tvReferenciaA = findViewById<TextView>(R.id.tvReferenciaA)
        val tvClaveRastreoA = findViewById<TextView>(R.id.tvClaveRastreoA)
        val tvEjecutoA = findViewById<TextView>(R.id.tvEjecutoA)

        val nombre_beneficiario = intent.getStringExtra("nombre_beneficiario")
        val cuenta = intent.getStringExtra("cuenta_clave_beneficiario")
        val rfc = intent.getStringExtra("rfc_beneficiario")
        val importe = intent.getStringExtra("importe")
        val fecha_ejecucion = intent.getStringExtra("fecha_ejecucion")
        val referencia = intent.getStringExtra("numero_referencia")
        val clave_rastreo = intent.getStringExtra("clave_rastreo")
        val ejecuto = intent.getStringExtra("ejecuto")

        tvNombreBeneficiario.text = nombre_beneficiario
        tvRfc.text = rfc
        tvClave.text = cuenta
        tvImporteA.text = importe
        tvFechaA.text = fecha_ejecucion
        tvReferenciaA.text = referencia
        tvClaveRastreoA.text = clave_rastreo
        tvEjecutoA.text = ejecuto

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
