package com.example.aeo.activities

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.DisplayMetrics
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.aeo.R
import com.example.aeo.entity.tb_carta_instruccion
import com.example.aeo.entity.tb_inicio_ci
import com.example.aeo.entity.tb_smartphone
import com.example.aeo.entity.tb_usuario
import com.example.aeo.model.models.DataCI
import com.example.aeo.utilities.MessageError
import com.example.aeomovil.model.response.LoginResponse
import com.example.aeomovil.task.APIService
import com.example.aeomovil.utilities.Utils
import com.google.gson.Gson
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login_detalle.*
import kotlinx.android.synthetic.main.loading_to_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginActivity : AppCompatActivity() {

    var dialog : AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        dialog = SpotsDialog.Builder().setContext(this).build()

        edtUsuario.requestFocus()

        edtContraseña.setOnClickListener {
            enableUIElements(true)

        }
        //icLogin.layoutParams.height = resources.getDimension(R.dimen.Large).toInt()
        //icLogin.layoutParams.width = resources.getDimension(R.dimen.Large).toInt()
        edtUsuario.setOnClickListener {
            enableUIElements(true)
        }
        /*btnIniciar.setOnClickListener {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(intent)
            /*enableUIElements(true)
            loadingCarga.visibility = VISIBLE
            contentLogin.visibility = GONE
            avi.show()
            searchLogin(edtUsuario.text.toString(), edtContraseña.text.toString())*/
        }*/
        doAsync {
            val consualtaUsuario = SCPMobileApp.tablaUsuario.usuario().getUsuario()
            for (i in 0 until consualtaUsuario.size){
                val usuario = consualtaUsuario[i].usuario
                val nombre_usuario = consualtaUsuario[i].nombre_usuario
                Log.e("ConsultaUsuario","$usuario")
                Log.e("User",usuario)
                onComplete {
                    Log.e("User",usuario)
                    if (usuario == "" || usuario == null){
                       Log.e("User","No hay usuario")
                    }else{
                        Log.e("User","SI hay usuario")
                        tvNombre.visibility = VISIBLE
                        tvNombre.setText(nombre_usuario)
                        edtLUsuario.visibility = GONE
                        edtUsuario.setText(usuario)
                        edtContraseña.requestFocus()
                        btnIniciar.visibility = VISIBLE
                        tvCambiarUsuario.visibility = VISIBLE
                    }
                }
            }
        }
        tvCambiarUsuario.setOnClickListener {
            edtUsuario.requestFocus()
            tvNombre.visibility = GONE
            tvNombre.setText("")
            edtLUsuario.visibility = VISIBLE
            edtUsuario.setText("")
            btnIniciar.visibility = GONE
            tvCambiarUsuario.visibility = GONE
        }

        btnIniciar.setOnClickListener OnClickListener@{
            if(edtUsuario.text!!.isEmpty() || edtContraseña.text!!.isEmpty()){
                Utils.messageError(this, MessageError.VALIDA_CAMPOS)
            }else{
                if(Utils.veryfyAvailableNetwork(this)){
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    //imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)

                    dialog?.setMessage("Autenticando..")
                    dialog?.setCancelable(false)
                    dialog?.apply {
                        show()
                    }
                    searchLogin(edtUsuario.text.toString(), edtContraseña.text.toString())
                    //loadingCarga

                }else{
                    doAsync {
                        val consualtaUsuario = SCPMobileApp.tablaUsuario.usuario().getUsuario()
                        for (i in 0 until consualtaUsuario.size){
                            val id_operardor = consualtaUsuario[i].id_operador
                            val usuario = consualtaUsuario[i].usuario
                            val password = consualtaUsuario[i].password
                            val nombre_usuario = consualtaUsuario[i].nombre_usuario
                            val apellido_paterno = consualtaUsuario[i].apellido_paterno
                            val apellido_materno = consualtaUsuario[i].apellido_materno
                            val tipo_acceso = consualtaUsuario[i].tipo_acceso
                            val operacion_usuario = consualtaUsuario[i].operacion_usuario
                            val clave_operacion = consualtaUsuario[i].clave_operacion

                            Log.e("ConsultaUsuario","$usuario")
                            Log.e("ConsultaUsuario","$password")
                            onComplete {
                                Log.e("Tag", edtUsuario.text.toString())
                                Log.e("Tag", edtContraseña.text.toString())
                                if (edtUsuario.text.toString() == usuario && edtContraseña.text.toString() == password){
                                    val sharedPreference =  getSharedPreferences("mi_app_aeo",Context.MODE_PRIVATE)
                                    var editor = sharedPreference.edit()

                                    editor.putString("id_operador", id_operardor.toString())
                                    editor.putString("usuario", "Hola")
                                    editor.putString("apellidoP", apellido_paterno)
                                    editor.putString("apellidoM", apellido_materno)
                                    editor.putString("aperacion_usuario", operacion_usuario)

                                    Log.e("Tag", id_operardor.toString())
                                    dialog?.dismiss()
                                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                                if (edtUsuario.text.toString() == usuario && edtContraseña.text.toString() != password) {
                                    Utils.messageError(applicationContext, MessageError.CONTRASEÑA_INCORECTA)
                                }
                                if (edtUsuario.text.toString() != usuario && edtContraseña.text.toString() == password) {
                                    Utils.messageError(applicationContext, MessageError.CONTRASEÑA_INCORECTA)
                                }else{
                                    Utils.messageError(applicationContext, MessageError.DATOS_INVALDO)
                                }
                            }
                        }
                    }

                    //Toast.makeText(this, "No cuenta con conexion a internet", Toast.LENGTH_LONG).show()
                }
            }

        }

    }

    private fun searchLogin(usr:String, pass:String){
        Log.e("Tag", "Consuminedo el servicio")
        doAsync{
            val call = getRetrofit().create(APIService::class.java).login(usr, pass, "351872093080841").execute()
            val response = call.body() as LoginResponse

            Log.e("Tag", "----->"+call)

            uiThread {
                if(response.estatus == "true" && response.smartphone.estatus == "true"){
                    initCharacter(response)
                }else{
                    if(response.estatus == "true" && response.smartphone.estatus == "false"){
                        Utils.messageError(applicationContext, MessageError.DISPOSITIVO_NO_REGISTRADO)
                    }else{
                        if(response.estatus == "false" && response.smartphone.estatus == "true"){
                            Utils.messageError(applicationContext, MessageError.CONTRASEÑA_INCORECTA)
                        }else{
                            Utils.messageError(applicationContext, MessageError.DATOS_INVALDO)
                        }
                    }
                    dialog?.dismiss()
                }
            }
        }
    }

    private fun initCharacter (response: LoginResponse){

        val sharedPreference =  getSharedPreferences("mi_app_aeo",Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()

        editor.putString("id_operador", response.resultado.idOperador)
        editor.putString("usuario", response.resultado.name)
        editor.putString("apellidoP", response.resultado.apellidoP)
        editor.putString("apellidoM", response.resultado.apellidoM)
        editor.putString("aperacionU", response.resultado.operacionU)
        editor.putString("ruta_imagen", response.resultado.rutaImagen)
        editor.putString("fecha_imagen", response.resultado.fechaImagen)

        Log.e("Tag", "Id -->"+response.resultado.idOperador)
        Log.e("Tag", "-->"+response.resultado.name)
        Log.e("Tag", "-->"+response.resultado.apellidoP)
        //editor.putString("id_smartphone", response.smartphone.datosSmart.idSmart)
        editor.apply()
        deleteData(response)
        dialog?.dismiss()
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)


        finish()
    }

    private fun deleteData(paramData: LoginResponse) {
        doAsync {
            SCPMobileApp.tablaUsuario.usuario().deleteUsuario()
            SCPMobileApp.tablaSmartphone.smartphone().deleteSmartphone()
            onComplete {
                Log.e("DeleteData", "Datos Borrados")
                inserData(paramData)
            }
        }
    }

    private fun inserData(paramData: LoginResponse) {
        doAsync {

            var id_usuario = paramData.resultado.idUser.toInt()
            var id_operador = paramData.resultado.idOperador.toInt()
            var usuario = paramData.resultado.usuario
            var password = paramData.resultado.password
            var nombre_usuario = paramData.resultado.name
            var apellido_paterno = paramData.resultado.apellidoP
            var apellido_materno = paramData.resultado.apellidoM
            var tipo_acceso = paramData.resultado.tipoAcceso
            var operacion_usuario = paramData.resultado.operacionU
            var clave_operacion = paramData.resultado.claveOperacion
            var ruta_imagen = paramData.resultado.rutaImagen
            var fecha_imagen = paramData.resultado.fechaImagen

            if (apellido_materno == null){
                apellido_materno = ""
            }
            if (apellido_paterno == null){
                apellido_paterno = ""
            }
            if (ruta_imagen.isEmpty()){
                ruta_imagen = ""
            }
            if (fecha_imagen.isEmpty()){
                fecha_imagen = ""
            }
            if (clave_operacion.isEmpty()){
                clave_operacion = ""
            }

            var TBusuario = tb_usuario(
                id_usuario,
                id_operador,
                usuario,
                password,
                nombre_usuario,
                apellido_paterno,
                apellido_materno,
                tipo_acceso,
                operacion_usuario,
                clave_operacion,
                ruta_imagen,
                fecha_imagen
            )

            SCPMobileApp.tablaUsuario.usuario().addUsuario(TBusuario)

            var id_smartphone = paramData.smartphone.datosSmart.idSmart.toInt()
            var imei = paramData.smartphone.datosSmart.imei
            var marca = paramData.smartphone.datosSmart.marca
            var modelo = paramData.smartphone.datosSmart.modelo
            var descripcion = paramData.smartphone.datosSmart.descripcion

            var smartphone = tb_smartphone (
                id_smartphone,
                imei,
                marca,
                modelo,
                descripcion
            )

            SCPMobileApp.tablaSmartphone.smartphone().addSmartphone(smartphone)


            onComplete {
                Toast.makeText(this@LoginActivity, "Datos insertados correctamente", Toast.LENGTH_LONG).show()
                Log.e("InsertData", "Datos Insertados")
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun enableUIElements(enable: Boolean){
        app_bar.setExpanded(!enable)
        activity_Detalle.isNestedScrollingEnabled = !enable

        btnIniciar.visibility = VISIBLE

        //edtLContraseñaD.visibility = VISIBLE

    }

    private fun toasMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
    
    /*fun addUsuario(usuario:UsuarioEntity){
        doAsync {  }
    }*/
}
