package com.example.aeo.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.example.aeo.R
import kotlinx.android.synthetic.main.activity_splash.*

class Splash : AppCompatActivity() , Runnable {

    private  lateinit var  handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        simularCarga()

        iv_imagen_login.animate()
            .scaleX(2.0f)
            .scaleY(2.0f)
            .duration = 2700
    }

    private fun simularCarga() {
        handler = Handler()
        handler.postDelayed(this, 3000)
    }

    override fun run(){
        val  sharedPreference =  getSharedPreferences(
            "",
            Context.MODE_PRIVATE
        )
        val usrId = sharedPreference.getString("usr_id","")
        Log.i("ValorDeUsrId", "->" + usrId)
        if(!usrId.equals("")){
            val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra("nombre_usr", ""+usrId)
            startActivity(intent)
            finish()
        }else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}