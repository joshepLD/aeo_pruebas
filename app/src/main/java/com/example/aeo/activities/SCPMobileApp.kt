package com.example.aeo.activities

import android.app.Application
import androidx.room.Room
import com.example.aeo.database.*
import com.example.aeo.database.db_carta_instruccion
import www.tasisoft.com.db.db_areas_ci
import www.tasisoft.com.db.db_galeria_ci


class SCPMobileApp: Application() {

    companion object {
        lateinit var tablaCartaInstruccion: db_carta_instruccion
        lateinit var tablaAreasCi: db_areas_ci
        lateinit var tablaCasetasCi: db_casetas_ci
        lateinit var tablaEvidenciaCargaCi: db_evidencia_carga_ci
        lateinit var tablaEvidenciaCi: db_evidencia_ci
        //lateinit var tablaEvidenciaDescargaCi: db_evidencia_descarga_ci
        lateinit var tablaGaleriaCi:db_galeria_ci
        lateinit var tablaIncidenciaCi: db_evidencia_incidencia_ci
        lateinit var tablaInicioCi: db_inicio_ci
        lateinit var tablaLugarCargaCi: db_lugar_carga_ci
        lateinit var tablaLugarDescargaCi: db_lugar_descarga_ci
        lateinit var tablaRemolquesCi: db_remolques_ci
        lateinit var tablaRutaCi: db_ruta_ci
        lateinit var tablaSmartphone: db_smartphone
        lateinit var tablaUsuario: db_usuario
    }

    override fun onCreate() {
        super.onCreate()
        tablaCartaInstruccion =  Room.databaseBuilder(this, db_carta_instruccion::class.java, "carta_instrucciones_db").build()
        tablaAreasCi =  Room.databaseBuilder(this, db_areas_ci::class.java, "areas_ci_db").build()
        tablaCasetasCi =  Room.databaseBuilder(this, db_casetas_ci::class.java, "casetas_ci_db").build()
        tablaEvidenciaCargaCi =  Room.databaseBuilder(this, db_evidencia_carga_ci::class.java, "evidencia_carga_ci_db").build()
        tablaEvidenciaCi = Room.databaseBuilder(this, db_evidencia_ci::class.java, "evidencia_ci_db").build()
        //tablaEvidenciaDescargaCi = Room.databaseBuilder(this, db_evidencia_descarga_ci::class.java, "evidencia_descarga_ci_db").build()
        tablaGaleriaCi = Room.databaseBuilder(this, db_galeria_ci::class.java, "galeria_ci_db").build()
        tablaIncidenciaCi = Room.databaseBuilder(this, db_evidencia_incidencia_ci::class.java, "evidencia_incidencia_ci_db").build()
        tablaInicioCi = Room.databaseBuilder(this, db_inicio_ci::class.java, "inicio_ci_db").build()
        tablaLugarCargaCi = Room.databaseBuilder(this, db_lugar_carga_ci::class.java, "lugar_carga_ci_db").build()
        tablaLugarDescargaCi = Room.databaseBuilder(this, db_lugar_descarga_ci::class.java, "lugar_descarga_ci_db").build()
        tablaRemolquesCi = Room.databaseBuilder(this, db_remolques_ci::class.java, "remolques_ci_db").build()
        tablaRutaCi = Room.databaseBuilder(this, db_ruta_ci::class.java, "ruta_ci_db").build()
        tablaSmartphone = Room.databaseBuilder(this, db_smartphone::class.java, "smartphone_db").build()
        tablaUsuario = Room.databaseBuilder(this, db_usuario::class.java, "usuario_db").build()
    }
}