package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.aeo.R

class WebView : AppCompatActivity() {

    var mywebview: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        mywebview = findViewById<WebView>(R.id.webview)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Ejemplo de Evidencia"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val nUrlFoto:String=intent.getStringExtra("foto_ejemplo")
        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        Log.e("Pdf",nUrlFoto)
        mywebview!!.loadUrl("$nUrlFoto")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
