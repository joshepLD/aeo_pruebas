package com.example.aeo.activities
/*

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_sincronizacion.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import www.tasisoft.com.R
import www.tasisoft.com.activities.Encuesta
import www.tasisoft.com.adapter.AdapterSurveyNotSend
import www.tasisoft.com.dao.APIService
import www.tasisoft.com.db.*
import www.tasisoft.com.models.*
import www.tasisoft.com.utilities.SharedPreference
import www.tasisoft.com.utilities.Utils
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.onComplete
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class SincronizacionFragment : Fragment() {
    lateinit var progreesDialog: ProgressDialog
    var fechaDefaulCatalog: String = ""
    var fechaDefaulUser: String = ""
    var fechaDefaulSurvey: String = ""
    var fechaActual: String = ""
    var banderaDateUser: String = ""
    var banderaDateCatalog: String = ""
    var banderaDateSurvey: String = ""
    lateinit var surveyNotSed: ArrayList<ModelSurveySend>

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_sincronizacion, container, false)
        val imgSincronizationUsers =
            view.findViewById<LinearLayout>(R.id.LinaerSincronizationUsers)
        val imgSincronizationCatalogs =
            view.findViewById<LinearLayout>(R.id.LinearSincronizationCatalogs)
        val imgSincronizationSurvey =
            view.findViewById<LinearLayout>(R.id.LinaarSincronizationSurvey)
        recyclerView = view.findViewById(R.id.recyclerEncuestasNoEnviadas)
        val textFecheEnvioSincronizacion =
            view.findViewById<TextView>(R.id.textFechaSincronizacionEnvio)

        imgSincronizationUsers.setOnClickListener {
            if (context?.let { it1 -> Utils.verifyAvailableNetwork(it1) }!!) {
                showDateUser()
                getUser()
            } else {
                toasMessage("Verifique su conexión a internet")
            }
        }

        imgSincronizationCatalogs.setOnClickListener {
            if (context?.let { it1 -> Utils.verifyAvailableNetwork(it1) }!!) {
                showDateCatalog()
                getCatalog()
            } else {
                toasMessage("Verifique su conexión a internet")
            }
        }

        imgSincronizationSurvey.setOnClickListener {
            if (context?.let { it1 -> Utils.verifyAvailableNetwork(it1) }!!) {
                showDateSurvey()
                getSurvey()
            } else {
                toasMessage("Verifique su conexión a internet")
            }
        }


        getTitleDateUser()
        getTitleDateCatalog()
        getTitleDateSurvey()
        getFechaEnvio()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSurveyNotSend()
        deleteSurveyNotDate()
        surveyNotSed = ArrayList()
        surveyNotSed.clear()
    }


    //Obtener fecha de sincronizacion de usuarios
    private fun getTitleDateUser() {
        doAsync {
            val fecha = Encuesta.databaseSincronizationDate.sincronizationDao().getDateId(1)
            uiThread {
                var fechaUsuario = ""
                for (i in 0 until fecha.size) {
                    fechaUsuario = fecha[i].fechaSincronizacion
                    tVDateUser.setText("Fecha: " + fechaUsuario)
                    banderaDateUser = fechaUsuario
                }
                onComplete {
                    if (fechaUsuario.equals("2000-01-01 01:30:00")) {
                        tVDateUser.setText("")
                    } else {
                        tVDateUser.setText("Fecha: " + fechaUsuario)
                    }
                }
            }
        }
    }


    //Obtener fecha de sincronizacion de catalogos
    private fun getTitleDateCatalog() {
        doAsync {
            val fecha = Encuesta.databaseSincronizationDate.sincronizationDao().getDateId(2)
            uiThread {
                var dateCatalog = ""
                for (i in 0 until fecha.size) {
                    dateCatalog = fecha[i].fechaSincronizacion
                    banderaDateCatalog = dateCatalog
                }
                onComplete {
                    if (dateCatalog.equals("2000-01-01 01:30:00")) {
                        tVDateCatalogs.setText("")
                    } else {
                        tVDateCatalogs.setText("Fecha: " + dateCatalog)
                    }
                }
            }
        }
    }

    //Obtener fecha de sincronizacion de encuestas
    private fun getTitleDateSurvey() {
        doAsync {
            val fecha = Encuesta.databaseSincronizationDate.sincronizationDao().getDateId(3)
            uiThread {
                var dateSurvey = ""
                for (i in 0 until fecha.size) {
                    dateSurvey = fecha[i].fechaSincronizacion
                    banderaDateSurvey = dateSurvey
                }
                onComplete {
                    if (dateSurvey.equals("2000-01-01 01:30:00")) {
                        tVDateSurvey.setText("")
                    } else {
                        tVDateSurvey.setText("Fecha: " + dateSurvey)
                    }
                }
            }
        }
    }

    private fun getFechaEnvio() {
        doAsync {
            val fechaEnvio = Encuesta.databaseSincronizationDate.sincronizationDao().getDateId(4)
            uiThread {
                var fecha = ""
                fechaEnvio.forEach {
                    fecha = it.fechaSincronizacion
                }
                onComplete {
                    textFechaSincronizacionEnvio.setText("Fecha de sincronización "+fecha)
                }
            }
        }
    }

    //Sincronizacion de usuarios
    private fun getUser() {
        progreesDialog = ProgressDialog(context)
        progreesDialog.setTitle("Sincronizando.....")
        progreesDialog.setCancelable(true)
        progreesDialog.show()
        doAsync {
            Log.d("TAG", "fechaEnvioUser " + fechaDefaulUser)
            val call = retrofitBase().create(APIService::class.java).getAllUsers(fechaDefaulUser).execute()
            progreesDialog.dismiss()
            try {
                if (call.isSuccessful) {
                    val response = call.body() as ResponseUsuarios
                    Log.v("JSONUSERDATA", "$response")
                    if (response.usuariosResponse.isEmpty()) {
                        uiThread {
                            toasMessage("Por el momento no hay información por actualizar")
                        }
                    } else {
                        response.usuariosResponse.forEach {
                            var idUsuario = it.idUsuario
                            var usuarioNombre = it.nombreUsuario
                            var usuarioCorreo = it.correoUsuario
                            var usuarioContrasenia = it.contraseñia
                            var usuarioBorrado = it.borrado

                            var usuarios = Users(
                                0,
                                idUsuario.toUpperCase(),
                                usuarioNombre,
                                usuarioCorreo,
                                usuarioContrasenia,
                                usuarioBorrado
                            )
                            Encuesta.databaseUsers.usuarioDao().insertUsers(usuarios)
                        }

                        uiThread {
                            toasMessage("La sincronización de usuarios fue exitosamente")
                            val formater = SimpleDateFormat("yyyy-MM-dd H:mm:ss")
                            var formateDate = formater.format(Date())
                            fechaActual = formateDate
                            insertDateUser(fechaActual)
                        }
                    }
                } else {
                    uiThread {
                        toasMessage("Algo salio mal intente nuevamente! ")
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    //sincronizacion de catalogos
    private fun getCatalog() {
        progreesDialog = ProgressDialog(context)
        progreesDialog.setTitle("Sincronizando.....")
        progreesDialog.setCancelable(true)
        progreesDialog.show()
        doAsync {
            Log.d("TAG", "FechaEnvioCatalog " + fechaDefaulCatalog)
            val call = retrofitBase().create(APIService::class.java).getAllCatalogs(fechaDefaulCatalog).execute()
            progreesDialog.dismiss()
            try {
                if (call.isSuccessful) {
                    val response = call.body() as ResponseCatalogos
                    Log.v("DATACATALOG ", "${response.catalogoIdioma}")
                    if (response.catalogoIdioma.isEmpty() && response.catalogoUnidadNegocio.isEmpty() && response.catalogoPaises.isEmpty()) {
                        uiThread {
                            toasMessage("Por el momento no hay información por actualizar")
                        }
                    } else {
                        //datos de idiomas
                        response.catalogoIdioma.forEach {
                            var lisIdIdioma = it.idIdioma
                            var lisNombreIdioma = it.nombreidioma
                            var lisBorradoIdioma = it.borrado

                            var idioma = LanguageCatalog(
                                lisIdIdioma,
                                lisNombreIdioma,
                                lisBorradoIdioma
                            )
                            Encuesta.databaseIdiomas.idiomaDao().insertIdioma(idioma)
                        }

                        //datos de unidad de negocio
                        response.catalogoUnidadNegocio.forEach {
                            var lisIdUnidadNegocio = it.idUnidad
                            var lisNombreUnidadNegocio = it.nombreUnidadDeNegocio
                            var lisBorradoNegocio = it.borrado
                            var lisLocalidad = it.localidades

                            var businessUnit = CatalogoUnidadDeNegocios(
                                lisIdUnidadNegocio,
                                lisNombreUnidadNegocio,
                                lisBorradoNegocio
                            )
                            Encuesta.databaseUnidadDeNegocio.unidadDeNegocioDao()
                                .insertUnidadDeNegocio(businessUnit)

                            lisLocalidad.forEach {
                                var listaId = it.idLocalidad
                                var listaNombre = it.nombreLocalidad
                                var listBorrado = it.borrado

                                var locationBusiness = LocationCatalog(
                                    listaId,
                                    listaNombre,
                                    lisIdUnidadNegocio,
                                    listBorrado
                                )
                                Encuesta.databaseLocalizacion.localizacionDao()
                                    .insertLocation(locationBusiness)
                            }

                        }

                        //datos de pais
                        response.catalogoPaises.forEach {
                            var lisIdPais = it.idPais
                            var lisNombrePais = it.nombrePais
                            var lisBorrado = it.borrado
                            val listEstados = it.estadosProvincias

                            var pais = CountryCatalog(lisIdPais, lisNombrePais.trim(), lisBorrado)
                            Encuesta.databasePais.paisDao().insertPaises(pais)

                            //Insertar estados por pais
                            listEstados.forEach {
                                var listIdPaisEstado = it.idEstado
                                var listNombreEstado = it.nombreEstado
                                var listBorrado = it.borrado

                                var estadoPais = EstadoPais(
                                    listIdPaisEstado, listNombreEstado.trim(),
                                    listBorrado, lisIdPais, lisNombrePais.trim()
                                )
                                Encuesta.databasePaisEstados.datosEstadosDao()
                                    .insertEstados(estadoPais)
                            }
                        }

                        var idSincronization = 1
                        var sharedPreferencesSincronizate =
                            context?.let { it1 -> SharedPreference(it1) }
                        sharedPreferencesSincronizate?.save(
                            "banderaSincronization",
                            idSincronization.toString().toInt()
                        )
                        uiThread {
                            toasMessage("Le sincronización de catalogos fue exitosamente")
                            val formater = SimpleDateFormat("yyyy-MM-dd H:mm:ss")
                            var formateDate = formater.format(Date())
                            fechaActual = formateDate
                            insertDateCatalog(fechaActual)
                        }
                    }
                } else {
                    uiThread {
                        toasMessage("Algo salio mal intente nuevamente! ")
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }


    //sincronizacion de encuestas
    private fun getSurvey() {
        progreesDialog = ProgressDialog(context)
        progreesDialog.setTitle("Sincronizando...")
        progreesDialog.setCancelable(true)
        progreesDialog.show()
        doAsync {
            Log.v( "fechaEnvioSurvey " , "$fechaDefaulSurvey")
            val call = retrofitBase().create(APIService::class.java).getAllSurveys(fechaDefaulSurvey).execute()
            onComplete {
                progreesDialog.dismiss()
                if (call.isSuccessful) {
                    val response = call.body() as ResponseSurveys
                    var gson = Gson()
                    var json = gson.toJson(response)
                    var jsonObject = JSONObject(json)
                    var objectArray: JSONArray = jsonObject.getJSONArray("Encuestas")
                    Log.v("RESPONSEAPI", "${objectArray.toString()}")
                    if (objectArray.length() == 0) {
                        uiThread {
                            toasMessage("Por el momento no hay información por actualizar")
                        }
                    } else {
                        //readFile(objectArray)
                        deleteOption(objectArray)
                        //inserDataSurvey(objectArray)
                    }
                } else {
                    uiThread {
                        toasMessage("Algo salio mal intente nuevamente! ")
                    }
                }
            }
        }
    }

    private fun deleteOption(objectArrayParam: JSONArray){
        doAsync {
            for (i in 0 until objectArrayParam.length()) {
                var sObjet: JSONObject = objectArrayParam.getJSONObject(i)
                val lisPreguntas = sObjet.getJSONArray("listaDePreguntas")
                for (g in 0 until lisPreguntas.length()) {
                    val sObjetPreguntas: JSONObject = lisPreguntas.getJSONObject(g)
                    val listIdPregunta = sObjetPreguntas.getInt("idPregunta")
                    Log.v("ELIMINAR","$listIdPregunta")
                    if (!fechaDefaulSurvey!!.equals("2000-01-01 01:30:00")) {
                        Log.v("ELIMINAR","$listIdPregunta")
                        Encuesta.databaseQuestionOptions.questionOptionDao().deleteOption(listIdPregunta)
                    }
                }
            }
            onComplete {
                Log.v("ELIMINAR"," TERMINADO  |°-°| ")
                inserDataSurvey(objectArrayParam)
            }
        }
    }


    private fun inserDataSurvey(objectArrayParam: JSONArray) {
        progreesDialog = ProgressDialog(context)
        progreesDialog.setTitle("Insertando datos...")
        progreesDialog.setCancelable(false)
        progreesDialog.show()
        Log.v("INSERDATOS","INSERTARDATOS")
        doAsync {
            for (i in 0 until objectArrayParam.length()) {
                var sObjet: JSONObject = objectArrayParam.getJSONObject(i)
                val listnombreEncuesta = sObjet.get("nombreEncuesta").toString()
                val listidEncuesta = sObjet.getInt("idEncuesta")
                val listidUnidadDeNegocioEncuesta = sObjet.getInt("idUnidadDeNegocio")
                //val listDescripcionEncuesta = sObjet.getString("Descripcion").toString()
                val listBorradoEncuesta: Boolean = sObjet.getBoolean("borrado")
                val listMinitoEncuesta = sObjet.getInt("minutos")
                val lisPreguntas = sObjet.getJSONArray("listaDePreguntas")

                //insert db local
                val dataSurvery = Surveys(
                    listidEncuesta,
                    listnombreEncuesta,
                    listidUnidadDeNegocioEncuesta,
                    "",
                    listBorradoEncuesta,
                    listMinitoEncuesta
                )
                Encuesta.databaseEncuestas.encuestasDAO().insertSurvey(dataSurvery)

                //for par iterar preguntas
                for (g in 0 until lisPreguntas.length()) {
                    val sObjetPreguntas: JSONObject = lisPreguntas.getJSONObject(g)
                    val listBorradoPregunta: Boolean = sObjetPreguntas.getBoolean("borrado")
                    val listTipoPregunta = sObjetPreguntas.get("tipoPregunta").toString()
                    val listidTipoPregunta = sObjetPreguntas.getInt("idTipoPregunta")
                    val listIdPregunta = sObjetPreguntas.getInt("idPregunta")
                    val listIdIdioma = sObjetPreguntas.getInt("idIdioma")
                    //val listonEsNumerico = sObjetPreguntas.getInt("noEsNumerico").toString()
                    //val listnoLongitud = sObjetPreguntas.getInt("noLongitud").toString()
                    val listordenEncuesta = sObjetPreguntas.getInt("ordenEnEncuesta")
                    val listPseudoID = sObjetPreguntas.get("PseudoID").toString()
                    val listNombrePregunta = sObjetPreguntas.get("nombrePregunta").toString()
                    val listEsRequerido = sObjetPreguntas.getBoolean("esRequerido")
                    val listTieneCampoOtros = sObjetPreguntas.getBoolean("tieneCampoOtros")
                    val listTextoOtros = sObjetPreguntas.get("textoOtros").toString()
                    val listVisibleSi = sObjetPreguntas.get("visibleSi").toString()
                    val listRequeridoSi = sObjetPreguntas.get("requeridoSi").toString()
                    val listNombreSiEsTrue = sObjetPreguntas.get("nombreSiEsTrue").toString()
                    val listNombreSiEsFalse = sObjetPreguntas.get("nombreSiEsFalse").toString()
                    val listTieneSeleccionarTodos: Boolean = sObjetPreguntas.getBoolean("tieneSeleccionarTodos")
                    val listOpcionesVisiblesSi = sObjetPreguntas.get("opcionesVisiblesSi").toString()
                    val listCnEsNumerico = sObjetPreguntas.getBoolean("cnEsNumerico")
                    val listNoLongitud = sObjetPreguntas.getInt("noLongitud")
                    val listDsTextoMenor = sObjetPreguntas.get("dsTextoMenor").toString()
                    val listDsTextoMayor = sObjetPreguntas.get("dsTextoMayor").toString()
                    val listOpciones = sObjetPreguntas.getJSONArray("Opciones")

                    val preguntasInsert = Questions(
                        listIdPregunta,
                        listBorradoPregunta,
                        listTipoPregunta,
                        listidTipoPregunta,
                        listIdIdioma,
                        listordenEncuesta,
                        listPseudoID,
                        listNombrePregunta,
                        listEsRequerido,
                        listTieneCampoOtros,
                        listTextoOtros,
                        listVisibleSi,
                        listRequeridoSi,
                        "",
                        "",
                        true,
                        listOpcionesVisiblesSi,
                        listidEncuesta,
                        listCnEsNumerico,
                        listNoLongitud,
                        listDsTextoMenor,
                        listDsTextoMayor
                    )
                    Encuesta.databasePreguntas.preguntasDAO()
                        .insertPreguntas(preguntasInsert)

                    for (s in 0 until listOpciones.length()) {
                        val sObjetPreguntasOpciones: JSONObject = listOpciones.getJSONObject(s)
                        val listOptionPreguntaValor = sObjetPreguntasOpciones.getString("valor")
                        val listOptionPreguntaTexto = sObjetPreguntasOpciones.getString("texto")
                        val listOptionPreguntaImagen = sObjetPreguntasOpciones.getString("imagen")

                        var opcionesPreguntas = QuestionsOptions(
                            0, listIdPregunta,
                            listIdIdioma, listOptionPreguntaValor.trim(),
                            listOptionPreguntaTexto.trim()
                        )

                        Encuesta.databaseQuestionOptions.questionOptionDao()
                            .insertQuestionOption(opcionesPreguntas)
                    }
                }
            }

            var idSincronization = 1
            var sharedPreferencesSincronizate =
                context?.let { it1 -> SharedPreference(it1) }
            sharedPreferencesSincronizate?.save(
                "banderaSincronizationSurvey",
                idSincronization.toString().toInt()
            )
            uiThread {
                progreesDialog.dismiss()
                toasMessage("La sincronización de encuestas fue exitosamente")
                val formater = SimpleDateFormat("yyyy-MM-dd H:mm:ss")
                var formateDate = formater.format(Date())
                fechaActual = formateDate
                insertDateSurvey(fechaActual)
            }
        }
    }


    private fun deleteOptiones(idPregunta: Int) {
        Log.v("IdPreguntaDelete ", "$idPregunta")
        if (!fechaDefaulSurvey!!.equals("2000-01-01 01:30:00")) {
            doAsync {
                Encuesta.databaseQuestionOptions.questionOptionDao().deleteOption(idPregunta)
            }
        }
    }


    //obtener la fecha de sincronizacion para usuarios valor 1
    private fun showDateUser() {
        doAsync {
            val titleDate =
                Encuesta.databaseSincronizationDate.sincronizationDao().getSincronizationDate()
            for (i in 0 until titleDate.size) {
                var titleUSer = titleDate[0].fechaSincronizacion
                fechaDefaulUser = titleUSer
                Log.d("TAG", "dataUser " + titleUSer + " " + fechaDefaulUser)
            }
        }
    }


    //obtener la fecha de sincronizacion para usuarios valor 2
    private fun showDateCatalog() {
        doAsync {
            val titleDate =
                Encuesta.databaseSincronizationDate.sincronizationDao().getSincronizationDate()
            for (i in 0 until titleDate.size) {
                var titleCatalog = titleDate[1].fechaSincronizacion
                fechaDefaulCatalog = titleCatalog
                Log.d("TAG", "dataCatalog " + titleCatalog + " " + fechaDefaulCatalog)
            }
        }
    }


    //obtener la fecha de sincronizacion para usuarios valor 3
    private fun showDateSurvey() {
        doAsync {
            val titleDate =
                Encuesta.databaseSincronizationDate.sincronizationDao().getSincronizationDate()
            for (i in 0 until titleDate.size) {
                var titleSurvey = titleDate[2].fechaSincronizacion
                fechaDefaulSurvey = titleSurvey
                Log.d("TAG", "dataSurvey " + titleSurvey + " " + fechaDefaulSurvey)
            }
        }
    }


    //insertar fecha de sincronizacion para usuarios valor 1
    private fun insertDateUser(fechaUser: String) {
        Log.d("TAG", "InsertUser " + fechaUser)
        doAsync {
            val insertDate = SincronizationDate(1, fechaUser)
            Encuesta.databaseSincronizationDate.sincronizationDao()
                .insertSincronizationDate(insertDate)
            getTitleDateUser()
        }
    }


    //insertar fecha de sincronizacion para catalogos valor 2
    private fun insertDateCatalog(fechaCatalog: String) {
        Log.d("TAG", "InsertCatalog " + fechaCatalog)
        doAsync {
            val insertDate = SincronizationDate(2, fechaCatalog)
            Encuesta.databaseSincronizationDate.sincronizationDao()
                .insertSincronizationDate(insertDate)
            getTitleDateCatalog()
        }
    }


    //insertar fecha de sincronizacion para encuestas valor 3
    private fun insertDateSurvey(fechaSurvey: String) {
        Log.d("TAG", "InsertSurvey " + fechaSurvey)
        doAsync {
            val insertDate = SincronizationDate(3, fechaSurvey)
            Encuesta.databaseSincronizationDate.sincronizationDao()
                .insertSincronizationDate(insertDate)
            getTitleDateSurvey()
        }
    }


    //Eliminar encuestas sin fechas
    private fun deleteSurveyNotDate() {
        doAsync {
            val surveyDelete =
                Encuesta.databaseSurveyInformation.surveyInformationDao().deleteDateTableNot("")
        }
    }


    //mostrar encuestas no enviadas
    private fun getSurveyNotSend() {
        var jsonArraySurvey = JSONArray()
        var surveyObjet = JSONObject()
        doAsync {
            val listSurvey = Encuesta.databaseSurveyInformation.surveyInformationDao()
                .getAllSurveyInformationIdNotSend(0,"En proceso para envio")
            uiThread {
                try {
                    for (i in 0 until listSurvey.size) {
                        val idTableInformacion = listSurvey[i].idEncuestaInformacion
                        val listnombreDeEncuesta = listSurvey[i].nombreDeEncuesta
                        val listidUsuarioCreacion = listSurvey[i].idUsuarioCreacion
                        val listidPaisEncuesta = listSurvey[i].idPaisEncuesta
                        val listcorreoElectronicoEncuestado = listSurvey[i].correoElectronicoEncuestado
                        val listnombreEncuestado = listSurvey[i].nombreEncuestado
                        val listidLocalidad = listSurvey[i].idLocalidad
                        val listiDUnidadDeNegocio = listSurvey[i].iDUnidadDeNegocio
                        val listidEncuesta = listSurvey[i].idEncuesta
                        val listfechaDeInicio = listSurvey[i].fechaDeInicio
                        val listfechaEnQueFueCompletada = listSurvey[i].fechaEnQueFueCompletada
                        val listlatitud = listSurvey[i].latitud
                        val listlongitud = listSurvey[i].longitud
                        val listIdIdioma = listSurvey[i].idIdioma
                        val listmedio = listSurvey[i].medio
                        val listaviso = listSurvey[i].aceptoAviso
                        val listMailing = listSurvey[i].aceptoMailing
                        val listEstadoEnviado = listSurvey[i].estadoEnviado

                        var objectSurvey = JSONObject()
                        try {
                            objectSurvey.put("idTablaInfomacion ", idTableInformacion)
                            objectSurvey.put("NombreDeEncuesta", listnombreDeEncuesta)
                            objectSurvey.put("IdUsuarioCreacion", listidUsuarioCreacion)
                            objectSurvey.put("IdPaisEncuesta", listidPaisEncuesta)
                            objectSurvey.put(
                                "CorreoElectronicoEncuestado",
                                listcorreoElectronicoEncuestado
                            )
                            objectSurvey.put("NombreEncuestado", listnombreEncuestado)
                            objectSurvey.put("IdLocalidad", listidLocalidad)
                            objectSurvey.put("IDUnidadDeNegocio", listiDUnidadDeNegocio)
                            objectSurvey.put("IdEncuesta", listidEncuesta)
                            objectSurvey.put("FechaDeInicio", listfechaDeInicio)
                            objectSurvey.put("FechaEnQueFueCompletada", listfechaEnQueFueCompletada)
                            objectSurvey.put("Latitud", listlatitud)
                            objectSurvey.put("Longitud", listlongitud)
                            objectSurvey.put("IdIdioma", listIdIdioma)
                            objectSurvey.put("Medio", listmedio)
                            objectSurvey.put("AceptaAviso", listaviso)
                            objectSurvey.put("AceptaMailing", listMailing)
                            objectSurvey.put("estadoEnviado", listEstadoEnviado)
                            jsonArraySurvey.put(objectSurvey)
                            Log.d("TAG", "surveyNotSend " + jsonArraySurvey)
                        } catch (ex: Exception) {

                        }
                    }
                    //formar json
                    var jsonSurvey = surveyObjet.put("encuestas", jsonArraySurvey)
                    val gson = GsonBuilder().create()
                    var jsonSurveyFormater = gson.fromJson<ModelSurveyNotSendResponse>(
                        jsonSurvey.toString(),
                        ModelSurveyNotSendResponse::class.java
                    )
                    linearLayoutManager = LinearLayoutManager(context)
                    recyclerView.layoutManager = linearLayoutManager
                    surveyNotSed = jsonSurveyFormater.listaEncuestas as ArrayList<ModelSurveySend>
                    recyclerView.adapter =
                        context?.let { it1 -> AdapterSurveyNotSend(surveyNotSed, it1) }

                } catch (ex: Exception) {
                }
            }
        }
    }


    private fun toasMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    private fun retrofitBase(): Retrofit {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(25, TimeUnit.SECONDS)
            .build()


        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(getString(R.string.urlGetUsuarios))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}*/
