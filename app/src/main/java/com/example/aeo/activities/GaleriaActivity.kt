package com.example.aeo.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.adapter.GaleriaAdapter
import com.example.aeo.model.response.EnvioGaleriaResponseB
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.activity_galeria.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import www.tasisoft.com.db.tb_galeria_ci
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.logging.Handler
import kotlin.collections.ArrayList


class  GaleriaActivity : AppCompatActivity() {

    private val TAKE_PHOTO_REQUEST = 101
    private val GALLERY = 1
    private val CAMERA = 2
    private var switchBotonVisualizar = true

    private  lateinit var  handler: Handler


    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var id_evidencia: String
    private lateinit var tipo_evidencia: String
    private lateinit var fotoBitmap: Bitmap

    var list: ArrayList<Any> = ArrayList()

    var imgBitmap: Bitmap? = null

    lateinit var recyclerView: RecyclerView
    private var modelGaleria = mutableListOf<tb_galeria_ci>()

    lateinit var adapterGaleria: GaleriaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galeria)

        supportActionBar?.hide()
        recyclerView = findViewById(R.id.recyclerImagen)

        id_evidencia = intent.getStringExtra("id_evidencia")
        tipo_evidencia = intent.getStringExtra("tipo_evidencia")

        recyclerImagen.visibility = GONE
        imgCapturada.visibility = VISIBLE
        lyComentarios.visibility = VISIBLE
        txtVisualizarF.setText("visualizar fotos")

        Log.e("id_evidencia -->", id_evidencia)

        //Tomar Foto
        buttonTomarF.setOnClickListener {
            recyclerImagen.visibility = GONE
            imgCapturada.visibility = VISIBLE
            lyComentarios.visibility = VISIBLE
            //Consulta para revision de  Imagenes en la base de Datos


            //doAsync {
              //  var consulta = SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagen()
                //onComplete {
                  //  modelGaleria.addAll(consulta)
                    Log.v("total___ ", modelGaleria.size.toString())
                    //if (modelGaleria.size <= 30){
                        permisoCamara()
                   // }else{
                     //   toasMessage("Ha exedido el limite de fotos tomadas")
                    //}
                //}
            //}
        }
        buttonCheck.setOnClickListener {
            if (textContenido.text.isNotEmpty()){
                val bitmap = (imgCapturada.getDrawable() as BitmapDrawable).getBitmap()
                Log.e("Bitmap Uno",bitmap.toString())

                val stream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val image = stream.toByteArray()

                val encodedString: String = Base64.getEncoder().encodeToString(image)

                val decodedBytes = Base64.getDecoder().decode(encodedString)
                //val decodedString = String(decodedBytes)
                Log.e("base64",encodedString)
                //textContenido.setText(encodedString)
                addGaleria(tb_galeria_ci(0, encodedString, textContenido.text.toString(), id_evidencia, tipo_evidencia , 0))
            }else{
                toasMessage("Es necesario la imagen y el texto ")
            }
        }
        lyVisualizarFoto.setOnClickListener {
            if (switchBotonVisualizar == true){
                recyclerImagen.visibility = VISIBLE
                imgCapturada.visibility = GONE
                lyComentarios.visibility = GONE
                switchBotonVisualizar = false
                txtVisualizarF.setText("ocultar fotos")
            }else{
                recyclerImagen.visibility = GONE
                imgCapturada.visibility = VISIBLE
                lyComentarios.visibility = VISIBLE
                switchBotonVisualizar = true
                txtVisualizarF.setText("visualizar fotos")
            }
        }
        btnEnviar.setOnClickListener {
            Toast.makeText(this@GaleriaActivity, "Clic Enviar", Toast.LENGTH_SHORT).show()

            doAsync {
                var galeria_envio = SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagenId(id_evidencia, tipo_evidencia)
                var datos_ci = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
                var datos_usuario = SCPMobileApp.tablaUsuario.usuario().getUsuario()

                var id_operador = "4"//datos_usuario[0].id_operador
                var id_carta = datos_ci[0].id_carta
                var folio_carta = datos_ci[0].folio
                var tipo_carta = datos_ci[0].operacion

                onComplete {
                    try {
                        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
                        val networkInfo  = connectivityManager.activeNetworkInfo

                        if (networkInfo != null && networkInfo.isConnected) {
                            // Si hay conexión a Internet en este momento
                            Log.e("Tag---> ","Si hay conexión a Internet en este momento")
                            for (i in 0 until galeria_envio.size) {
                                var tipo_evidencia = galeria_envio[i].tipo_evidencia
                                var id_evidencia = galeria_envio[i].id_evidencia
                                var comentario_imagen = galeria_envio[i].comentario_imagen
                                var imagen = galeria_envio[i].imagen
                                var base64 = "data:image/jpge:base64,${imagen}"

                                Log.e("Tag---> ",base64)

                                envioGaleria(
                                    id_operador,
                                    id_carta.toString(),
                                    folio_carta,
                                    tipo_carta,
                                    tipo_evidencia,
                                    id_evidencia,
                                    base64,
                                    comentario_imagen
                                )
                                Thread.sleep(3000)
                            }
                        } else {
                            // No hay conexión a Internet en este momento
                            Log.e("Tag---> ","No hay conexión a Internet en este momento")
                        }
                    } catch (e: IOException) {
                        Log.v("catch", "Hubo un error")
                    }
                }

                /*galeria_envio.forEach {
                    var tipo_evidencia = it.tipo_evidencia
                    var id_evidencia = it.id_evidencia
                    var comentario_imagen = it.comentario_imagen
                    var imagen = it.imagen
                    var base64 = "data:image/png;base64,${imagen}"

                    //getObjectArray(id_operador, id_carta.toString(), folio_carta, tipo_carta, tipo_evidencia, id_evidencia, base64, comentario_imagen)
                }*/
            }
            //onBackPressed()
        }
        //getImagen(id_evidencia, tipo_evidencia)
        getImagenDos(id_evidencia, tipo_evidencia)
        getObjectArray("1","34","12345","DOM","Carga","9","sdfghjxcfvgbhnjgh","dfghjgh")
    }

    private fun getObjectArray(idOperardor: String,
                               idCarta: String,
                               folioCarta: String,
                               tipoCarta: String,
                               tipoEvidencia: String,
                               idEvidencia: String,
                               base64: String,
                               comentarioImagen: String){
        Toast.makeText(this@GaleriaActivity, "ObjectArray", Toast.LENGTH_SHORT).show()
        var objectArray = JSONObject()
        objectArray.put("id_operador",idOperardor)
        objectArray.put("id_carta",idCarta)
        objectArray.put("folio_carta",folioCarta)
        objectArray.put("tipo_carta",tipoCarta)
        objectArray.put("tipo_evidencia",tipoEvidencia)
        objectArray.put("id_evidencia",idEvidencia)
        objectArray.put("evidence",base64)
        objectArray.put("comentario",comentarioImagen)

        Log.e("JSONObjec",""+objectArray)
        list.add(objectArray)
        Log.e("Array", list.toString())


    }

    private fun getImagenDos(id_parametro: String, tipo_parametro: String){
        doAsync {
            var galeria = SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagenId(id_parametro, tipo_parametro)
            Log.e("Tag", "getIncidencias")
            uiThread {
                setUpRecyclerView(galeria)

            }
        }
    }

    private  fun setUpRecyclerView(galeria: List<tb_galeria_ci>) {
        Log.e("Tag", "setUpRecyclerView")

        modelGaleria.addAll(galeria)

        linearLayoutManager = LinearLayoutManager(this@GaleriaActivity)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = GaleriaAdapter(modelGaleria,this@GaleriaActivity, id_evidencia)
        Log.e("DB","Datos recuperados")
    }

    private fun addGaleria(addGaleria: tb_galeria_ci){

        doAsync {
            SCPMobileApp.tablaGaleriaCi.galeriaDao().insertGaleria(addGaleria)
            onComplete {
                uiThread {
                    toasMessage("Alameceno dato")
                    textContenido.setText("")
                    imgCapturada.setImageDrawable(null)
                    //getImagen(id_evidencia, tipo_evidencia)
                    imgBitmap = null
                    //
                    modelGaleria.add(addGaleria)
                    Toast.makeText(this@GaleriaActivity, "Incidencia Agregada", Toast.LENGTH_LONG).show()

                    recyclerImagen.visibility = VISIBLE
                    imgCapturada.visibility = GONE
                    lyComentarios.visibility = GONE

                    getImagenDos(id_evidencia, tipo_evidencia)
                }
            }

        }
    }
    //
    private fun permisoCamara() {
        revisapermiso()
    }

    //Funcion para revision de los permisos de CAMARA
    fun revisapermiso() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), TAKE_PHOTO_REQUEST
            )
            Log.i("TAG", "Permisos Pedir---")
        }else{
            //Ya tiene permisos
            takePhotoFromCamera()
        }
    }

    //Funcion para llamado de camara
    private fun takePhotoFromCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
        //onProgressUpdate()
    }

    //Creacion del Bitmap
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY){
        }else if( requestCode == CAMERA){
            imgBitmap = data!!.extras!!.get("data") as Bitmap
            imgCapturada.setImageBitmap(imgBitmap)
        }
    }

    //Permisos denegados
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            TAKE_PHOTO_REQUEST -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhotoFromCamera()
            }else{
                Log.i("TAG", "No dio permiso")
            }
        }
    }

    private fun envioGaleria(idOperardor: String,
                             idCarta: String,
                             folioCarta: String,
                             tipoCarta: String,
                             tipoEvidencia: String,
                             idEvidencia: String,
                             base64: String,
                             comentarioImagen: String) {

        Log.e("Tag", "Servicioooooo")

        doAsync{
            val call = getRetrofit().create(APIService::class.java)
                .envioGaleriaB(idOperardor, idCarta, folioCarta, tipoCarta, tipoEvidencia, idEvidencia, base64, "base64", comentarioImagen).execute()
            var response = call.body() as EnvioGaleriaResponseB
            Log.e("Service",call.toString())
            //Log.e("Service",response.toString())
            uiThread{

                Log.e("Response",response.typeProccess)
                //Log.e("Imagen", evidence)
                if(response.typeProccess == "success"){
                    Toast.makeText(this@GaleriaActivity, "Datos Enviados", Toast.LENGTH_SHORT).show()

                }else{
                    Toast.makeText(this@GaleriaActivity, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun toasMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    //
    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }

}
