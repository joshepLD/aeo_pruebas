package com.example.aeo.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.aeo.R
import kotlinx.android.synthetic.main.activity_mapas.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MapasActivity : AppCompatActivity() {

    private lateinit var id_carta_descarga: String
    private lateinit var dato_descarga: String
    private lateinit var nombre_descarga: String
    private lateinit var direccion_descarga: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mapas)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Mapas"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        /*doAsync {
            Log.v("Consulta","")
            val consualta = SCPMobileApp.tablaLugarDescargaCi.lugar_descarga_ci().getAllLugarDescarga()
            for (i in 0 until consualta.size){
                id_carta_descarga = consualta[i].id_carta
                dato_descarga = consualta[i].datos
                nombre_descarga = consualta[i].nombre
                direccion_descarga = consualta[i].direccion
                uiThread {
                    Log.e("CLIENTE","$id_carta_descarga")
                }
            }
        }*/

        lnRuta.setOnClickListener{
            val intent = Intent(this, RutaDosActivity::class.java)
            startActivity(intent)
        }
        lnLugarCarga.setOnClickListener{
            val intent = Intent(this, ZonaCargaActivity::class.java)
            startActivity(intent)
        }
        /*lnInicio.setOnClickListener{
            val intent = Intent(this, ZonaInicioActivity::class.java)
            startActivity(intent)
        }*/
        lnLugarDescarga.setOnClickListener {  }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
