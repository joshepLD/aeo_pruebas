package com.example.aeo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aeo.R
import com.example.aeo.adapter.RemolquesAdapter
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.task.APIService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


class DescripcionCartaDos : AppCompatActivity() {

    private lateinit var idOperador: String
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayout:LinearLayoutManager

    private lateinit var folio: String
    private lateinit var operacion: String
    private lateinit var tipo_servicio: String
    private lateinit var tipo_viaje: String
    private lateinit var placa: String
    private lateinit var cliente: String
    private lateinit var cliente_paga: String
    private lateinit var numero_viaje: String
    private lateinit var carta_porte: String
    private lateinit var nombre_contacto_carga: String
    private lateinit var tel_contacto_carga: String
    private lateinit var horario_inicio: String
    private lateinit var horario_carga: String
    private lateinit var horario_descarga: String
    private lateinit var maniobras: String
    private lateinit var observaciones: String
    private lateinit var tipo_inicio: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_descripcion_carta_dos)
        recyclerView = findViewById(R.id.rvRemolques)

        /*doAsync {
            Log.v("Consulta","")
            val consualta = SCPMobileApp.database.carta_instruccion().getAllTasks()
             for (i in 0 until consualta.size){
                folio = consualta[i].folio
                operacion = consualta[i].operacion
                tipo_servicio = consualta[i].tipo_servicio
                tipo_viaje = consualta[i].tipo_viaje
                placa = consualta[i].placa
                cliente = consualta[i].cliente
                cliente_paga = consualta[i].cliente_paga
                numero_viaje = consualta[i].numero_viaje
                carta_porte = consualta[i].carta_porte
                nombre_contacto_carga = consualta[i].nombre_contacto_carga
                tel_contacto_carga = consualta[i].tel_contacto_carga
                horario_inicio = consualta[i].horario_inicio
                horario_carga = consualta[i].horario_carga
                horario_descarga = consualta[i].horario_descarga
                maniobras = consualta[i].maniobras
                observaciones = consualta[i].observaciones
                tipo_inicio = consualta[i].tipo_inicio
                uiThread {
                    Log.e("CLIENTE","$cliente")
                }
            }
        }*/
        //
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Carta de Instrucciones"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        //

        var tvFolio = findViewById<TextView>(R.id.tvFolioD)
        var tvTipoServicio = findViewById<TextView>(R.id.tvTipoServicioD)
        var tvTipoViaje = findViewById<TextView>(R.id.tvTipoViajeD)
        var tvCliente = findViewById<TextView>(R.id.tvClienteD)
        var tvNumeroViaje = findViewById<TextView>(R.id.tvNumeroViajeD)
        var tvCartaPorte = findViewById<TextView>(R.id.tvCartaPorteD)
        var tvNombreContacto = findViewById<TextView>(R.id.tvNombreContactoD)
        var tvNumeroContacto = findViewById<TextView>(R.id.tvNumeroContactoD)
        var tvHorarioInicio = findViewById<TextView>(R.id.tvHorarioInicioD)
        var tvHorarioCarga = findViewById<TextView>(R.id.tvHorarioCargaD)
        var tvHorarioDescarga = findViewById<TextView>(R.id.tvHorarioDescargaD)

        tvFolio.text = folio
        tvTipoServicio.text = tipo_servicio
        tvTipoViaje.text = tipo_viaje
        tvCliente.text = cliente
        tvNumeroViaje.text = numero_viaje
        tvCartaPorte.text = carta_porte
        tvNombreContacto.text = nombre_contacto_carga
        tvNumeroContacto.text = tel_contacto_carga
        tvHorarioInicio.text = horario_inicio
        tvHorarioCarga.text = horario_carga
        tvHorarioDescarga.text = horario_descarga
        //searchCI()
    }


    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
