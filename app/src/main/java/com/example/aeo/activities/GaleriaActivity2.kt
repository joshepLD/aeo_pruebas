package com.example.aeo.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import com.example.aeo.R
import com.example.aeo.WorkManager.WorkManagerSend
import com.example.aeo.adapter.GaleriaAdapter
import com.example.aeo.model.response.EnvioGaleriaResponseArray
import com.example.aeomovil.task.APIService
import kotlinx.android.synthetic.main.activity_galeria.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import www.tasisoft.com.db.tb_galeria_ci
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class GaleriaActivity2 : AppCompatActivity() {
    private val TAKE_PHOTO_REQUEST = 101
    private val GALLERY = 1
    private val CAMERA = 2
    private var switchBotonVisualizar = true

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var id_evidencia: String
    private lateinit var tipo_evidencia: String

    var list: ArrayList<Any> = ArrayList()
    var imgBitmap: Bitmap? = null

    lateinit var recyclerView: RecyclerView
    private var modelGaleria = mutableListOf<tb_galeria_ci>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galeria)

        supportActionBar?.hide()
        recyclerView = findViewById(R.id.recyclerImagen)

        id_evidencia = intent.getStringExtra("id_evidencia")
        tipo_evidencia = intent.getStringExtra("tipo_evidencia")

        recyclerImagen.visibility = View.GONE
        imgCapturada.visibility = View.VISIBLE
        lyComentarios.visibility = View.VISIBLE
        txtVisualizarF.setText("visualizar fotos")

        Log.e("id_evidencia -->", id_evidencia)

        //Funcion de Botonera
        buttonTomarF.setOnClickListener {
            recyclerImagen.visibility = View.GONE
            imgCapturada.visibility = View.VISIBLE
            lyComentarios.visibility = View.VISIBLE
            //doAsync {
            //  var consulta = SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagen()
            //onComplete {
            //  modelGaleria.addAll(consulta)
            Log.v("total___ ", modelGaleria.size.toString())
            //if (modelGaleria.size <= 30){
            permisoCamara()
            // }else{
            //   toasMessage("Ha exedido el limite de fotos tomadas")
            //}
            //}
            //}
        }

        buttonCheck.setOnClickListener {
            if (textContenido.text.isNotEmpty()) {
                val bitmap = (imgCapturada.getDrawable() as BitmapDrawable).getBitmap()
                Log.e("Bitmap Uno", bitmap.toString())

                val stream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val image = stream.toByteArray()

                val encodedString: String = Base64.getEncoder().encodeToString(image)

                val decodedBytes = Base64.getDecoder().decode(encodedString)
                //val decodedString = String(decodedBytes)
                Log.e("base64", encodedString)
                //textContenido.setText(encodedString)
                addGaleria(
                    tb_galeria_ci(
                        0,
                        encodedString,
                        textContenido.text.toString(),
                        id_evidencia,
                        tipo_evidencia,
                        0
                    )
                )
            } else {
                toasMessage("Es necesario la imagen y el texto ")
            }
        }

        lyVisualizarFoto.setOnClickListener {
            if (switchBotonVisualizar == true) {
                recyclerImagen.visibility = View.VISIBLE
                imgCapturada.visibility = View.GONE
                lyComentarios.visibility = View.GONE
                switchBotonVisualizar = false
                txtVisualizarF.setText("ocultar fotos")
            } else {
                recyclerImagen.visibility = View.GONE
                imgCapturada.visibility = View.VISIBLE
                lyComentarios.visibility = View.VISIBLE
                switchBotonVisualizar = true
                txtVisualizarF.setText("visualizar fotos")
            }
        }

        btnEnviar.setOnClickListener {
            doAsync {
                var galeria_envio = SCPMobileApp.tablaGaleriaCi.galeriaDao()
                    .getImagenId(id_evidencia, tipo_evidencia)
                var datos_ci = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
                var datos_usuario = SCPMobileApp.tablaUsuario.usuario().getUsuario()

                var id_operador = "4"//datos_usuario[0].id_operador
                var id_carta = datos_ci[0].id_carta
                var folio_carta = datos_ci[0].folio
                var tipo_carta = datos_ci[0].operacion

                onComplete {
                    try {
                        val connectivityManager =
                            getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
                        val networkInfo = connectivityManager.activeNetworkInfo
                        if (networkInfo != null && networkInfo.isConnected) {
                            // Si hay conexión a Internet en este momento
                            Log.e("Tag---> ", "Si hay conexión a Internet en este momento")
                            for (i in 0 until galeria_envio.size) {
                                var tipo_evidencia = galeria_envio[i].tipo_evidencia
                                var id_evidencia = galeria_envio[i].id_evidencia
                                var comentario_imagen = galeria_envio[i].comentario_imagen
                                var imagen = galeria_envio[i].imagen
                                var base64 = "data:image/jpge:base64,${imagen}"

                                var objectArray = JSONObject()
                                objectArray.put("id_operador", id_operador)
                                objectArray.put("id_carta", id_carta)
                                objectArray.put("folio_carta", folio_carta)
                                objectArray.put("tipo_carta", tipo_carta)
                                objectArray.put("tipo_evidencia", tipo_evidencia)
                                objectArray.put("id_evidencia", id_evidencia)
                                objectArray.put("evidence", base64)
                                objectArray.put("tipo_archivo", "base64")
                                objectArray.put("comentario", comentario_imagen)

                                Log.e("JSONObjec", "" + objectArray)
                                list.add(objectArray)
                            }
                            Log.e("Array", list.toString())
                            envioGaleria(list.toString())
                        } else {
                            // No hay conexión a Internet en este momento
                            Log.e("Tag---> ", "No hay conexión a Internet en este momento")
                        }
                    } catch (e: IOException) {
                        Log.v("catch", "Hubo un error")
                    }
                }

                /*galeria_envio.forEach {
                    var tipo_evidencia = it.tipo_evidencia
                    var id_evidencia = it.id_evidencia
                    var comentario_imagen = it.comentario_imagen
                    var imagen = it.imagen
                    var base64 = "data:image/png;base64,${imagen}"

                    Log.e("Prueba", tipo_evidencia)

                    getObjectArray(id_operador, id_carta.toString(), folio_carta, tipo_carta, tipo_evidencia, id_evidencia, base64, comentario_imagen)
                }*/
            }
        }
        getImagenDos(id_evidencia, tipo_evidencia)
    }


    override fun onBackPressed() {
        sendImg()
        //val intent = Intent(this, EvidenciasActivity::class.java)
        //startActivity(intent)
    }

    private fun sendImg() {
        doAsync {
            var datos_ci = SCPMobileApp.tablaCartaInstruccion.carta_instruccion().getAllCI()
            var galeria_envio = SCPMobileApp.tablaGaleriaCi.galeriaDao()
                .getImagenNotSend(id_evidencia, tipo_evidencia, 0)
            Log.v("Consulta", "$galeria_envio")

            if (galeria_envio.size != 0) {
                uiThread {
                    var     list: ArrayList<Any> = ArrayList()
                    galeria_envio.forEach {
                        var objectArray = JSONObject()
                        objectArray.put("id_operador", "4")
                        objectArray.put("id_carta", datos_ci[0].id_carta.toString())
                        objectArray.put("folio_carta", datos_ci[0].folio)
                        objectArray.put("tipo_carta", datos_ci[0].operacion)
                        objectArray.put("tipo_evidencia", it.tipo_evidencia)
                        objectArray.put("id_evidencia", it.id_evidencia)
                        objectArray.put("evidence", "data:image/jpge:base64,${it.imagen}")
                        objectArray.put("tipo_archivo", "base64")
                        objectArray.put("comentario", it.comentario_imagen)
                        list.clear()
                        list.add(objectArray)
                        Log.v("JSONObjec", "1 $list")
                        process(list)
                    }
                }
                onComplete {

                }
            }
        }
    }


    //Enviar datos a WorkManager
    private fun process(list: ArrayList<Any>) {

        var jsonAsnwer: String = list.toString()
        Log.v("JSONWPARAM ", " ${jsonAsnwer}")

        val data = Data.Builder()
            .putString("dataAnswer", jsonAsnwer)
            .build()

        //Restriccion contar con red
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        //solicitud
        val oneTimeRequest = OneTimeWorkRequest.Builder(WorkManagerSend::class.java)
            //.setInitialDelay(1,TimeUnit.MINUTES)
            .setInputData(data)
            .setConstraints(constraints)
            .setInitialDelay(10, TimeUnit.SECONDS)
            .setBackoffCriteria(BackoffPolicy.LINEAR, 40, TimeUnit.SECONDS)
            .addTag("EnvioDatosWorkManager_____")
            .build()

        //Poner en cola el trabajo
        WorkManager.getInstance(this).enqueue(oneTimeRequest)
        /*WorkManager.getInstance(this).enqueueUniqueWork(WORK_NAME,
            ExistingWorkPolicy.APPEND,oneTimeRequest)*/
    }


    //Agregar Eidencia a Base de Datos
    private fun addGaleria(addGaleria: tb_galeria_ci) {

        doAsync {
            SCPMobileApp.tablaGaleriaCi.galeriaDao().insertGaleria(addGaleria)
            onComplete {
                uiThread {
                    toasMessage("Alameceno dato")
                    textContenido.setText("")
                    imgCapturada.setImageDrawable(null)
                    //getImagen(id_evidencia, tipo_evidencia)
                    imgBitmap = null
                    //
                    modelGaleria.add(addGaleria)
                    Toast.makeText(this@GaleriaActivity2, "Incidencia Agregada", Toast.LENGTH_LONG)
                        .show()

                    recyclerImagen.visibility = View.VISIBLE
                    imgCapturada.visibility = View.GONE
                    lyComentarios.visibility = View.GONE

                    getImagenDos(id_evidencia, tipo_evidencia)
                }
            }

        }
    }

    //Consulta de Imagenes a la base de Datos
    private fun getImagenDos(id_parametro: String, tipo_parametro: String) {
        doAsync {
            var galeria =
                SCPMobileApp.tablaGaleriaCi.galeriaDao().getImagenId(id_parametro, tipo_parametro)
            Log.e("Tag -->", "getIncidencias")
            uiThread {
                setUpRecyclerView(galeria)

            }
        }
    }

    //Agregar imagenes a la vista
    private fun setUpRecyclerView(galeria: List<tb_galeria_ci>) {
        Log.e("Tag", "setUpRecyclerView")

        modelGaleria.addAll(galeria)

        linearLayoutManager = LinearLayoutManager(this@GaleriaActivity2)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = GaleriaAdapter(modelGaleria, this@GaleriaActivity2, id_evidencia)
        Log.e("DB", "Datos recuperados")
    }

    // Permisos de Camara
    private fun permisoCamara() {
        revisapermiso()
    }

    //Funcion para revision de los permisos de CAMARA
    fun revisapermiso() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), TAKE_PHOTO_REQUEST
            )
            Log.i("TAG", "Permisos Pedir---")
        } else {
            //Ya tiene permisos
            takePhotoFromCamera()
        }
    }

    //Funcion para llamado de camara
    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
        //onProgressUpdate()
    }

    //Creacion del Bitmap
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
        } else if (requestCode == CAMERA) {
            imgBitmap = data!!.extras!!.get("data") as Bitmap
            imgCapturada.setImageBitmap(imgBitmap)
        }
    }

    //Servicio envio de imageness
    private fun envioGaleria(arrayImagenes: String) {

        Log.e("Tag", "Servicioooooo")

        doAsync {
            val call = getRetrofit().create(APIService::class.java)
                .envioGaleriaArray(arrayImagenes).execute()
            var response = call.body() as EnvioGaleriaResponseArray
            Log.e("Service", call.toString())
            //Log.e("Service",response.toString())
            uiThread {
                //Log.e("Imagen", evidence)
                if (response.typeProccess == "success") {
                    Toast.makeText(this@GaleriaActivity2, "Datos Enviados", Toast.LENGTH_SHORT)
                        .show()
                    Log.e("Response", response.typeProccess)
                } else {
                    Toast.makeText(this@GaleriaActivity2, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //Complemento del servicio
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    //Toast Generico
    private fun toasMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}