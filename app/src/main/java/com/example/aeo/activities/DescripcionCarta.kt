package com.example.aeo.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.diegodobelo.expandingview.ExpandingItem
import com.diegodobelo.expandingview.ExpandingList
import com.example.aeo.R
import com.example.aeo.model.models.DataCI
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.task.APIService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class DescripcionCarta : AppCompatActivity() {
    private lateinit var mExpandingList: ExpandingList
    private lateinit var idOperador: String
    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayout:LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_descripcion_carta)
        mExpandingList = findViewById(R.id.expanding_list_main)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Carta de Instrucciones"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        //searchCI()

    }

}