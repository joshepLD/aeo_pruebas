package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_inicio_ci_entity")
class tb_inicio_ci (
    @PrimaryKey(autoGenerate = true)
    var id_carta:Int = 0,
    var datos:String = "",
    var nombre:String = "",
    var direccion:String = "",
    var tipo:String = ""
)