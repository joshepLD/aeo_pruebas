package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_remolques_ci_entity")
class tb_remolques_ci (
    @PrimaryKey(autoGenerate = true) val id: Int,
    var id_carta:Int = 0,
    var placa_remolque:String = "",
    var economico_remolque:String = ""
)