package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_smartphone_entity")
class tb_smartphone (
    @PrimaryKey(autoGenerate = true)
    var id_smartphone:Int = 0,
    var imei:String = "",
    var marca:String = "",
    var modelo:String = "",
    var descripcion:String = ""
)