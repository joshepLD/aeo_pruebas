package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_evidencia_descarga_ci_entity")
class tb_evidencia_descarga_ci (
    @PrimaryKey(autoGenerate = true)
    var folio_ci:String = "",
    var nombre:String = "",
    var ejemplo:String = "",
    var id_evidencia:String = ""
)