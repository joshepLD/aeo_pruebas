package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_lugar_carga_ci_entity")
class tb_lugar_carga_ci (
    @PrimaryKey (autoGenerate = true)
    var id_carta:Int = 0,
    var tipo:String = "",
    var lugar_de_inicio:String = "",
    var horario:String = "",
    var direccion:String = ""
)