package com.example.aeo.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_evidencia_incidencia_ci_entity")
class tb_evidencia_incidencia_ci (
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var nombre:String = ""

)