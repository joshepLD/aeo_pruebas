package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_usuario_entity")
class tb_usuario (
    @PrimaryKey(autoGenerate = true)
    var id_usuario:Int = 0,
    var id_operador:Int = 0,
    var usuario:String = "",
    var password:String = "",
    var nombre_usuario:String = "",
    var apellido_paterno:String = "",
    var apellido_materno:String = "",
    var tipo_acceso:String = "",
    var operacion_usuario:String = "",
    var clave_operacion:String = "",
    var ruta_imagen:String = "",
    var fecha_imagen:String = ""
)