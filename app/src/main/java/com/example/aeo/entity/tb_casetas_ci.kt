package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_casetas_ci_entity")
class tb_casetas_ci (
    @PrimaryKey (autoGenerate = true)
    var id_carta:Int = 0,
    var nombre_caseta:String = "",
    var alias_caseta:String = ""
)