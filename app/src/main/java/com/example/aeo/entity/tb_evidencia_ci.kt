package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_evidencia_ci_entity")
class tb_evidencia_ci (
    @PrimaryKey(autoGenerate = true)
    var id_carta:Int = 0,
    var datos:String = ""
)