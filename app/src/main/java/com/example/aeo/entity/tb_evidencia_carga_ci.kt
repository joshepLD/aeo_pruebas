package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_evidencia_carga_ci_entity")
class tb_evidencia_carga_ci (
    @PrimaryKey var id_carta:String = "",
    var nombre:String = "",
    var ejemplo:String = "",
    var id_evidencia:String = ""
)