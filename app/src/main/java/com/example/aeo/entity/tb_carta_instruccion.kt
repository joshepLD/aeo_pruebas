package com.example.aeo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_carta_instruccion_entity")
data class tb_carta_instruccion (
    @PrimaryKey (autoGenerate = true)
    var id_carta:Int = 0,
    var folio:String = "",
    var operacion:String = "",
    var operacion_abrv:String = "",
    var tipo_servicio:String = "",
    var tipo_viaje:String = "",
    var placa:String = "",
    var eco_camion:String = "",
    var nombre_cliente_carga:String = "",
    var cliente_paga: String = "",
    var numero_viaje: String = "",
    var carta_porte: String = "",
    var nombre_contacto_carga: String = "",
    var tel_contacto_carga: String = "",
    var horario_inicio: String = "",
    var horario_carga: String = "",
    var horario_descarga: String = "",
    var maniobras: String = "",
    var observaciones: String = "",
    var tipo_inicio: String = "",
    var evidencias_observaciones:String = "",
    var vista_gral:String = "",
    var vista_detallada:String = ""
)
