package www.tasisoft.com.db

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.RequestBuilder
import java.io.File

@Entity(tableName = "tb_galeria_ci_entity")
data class tb_galeria_ci(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "imagen") var imagen: String = "",
    @ColumnInfo(name = "comentario_imagen") var comentario_imagen: String = "",
    @ColumnInfo(name = "id_evidencia") var id_evidencia: String = "",
    @ColumnInfo(name = "tipo_evidencia") var tipo_evidencia: String = "",
    @ColumnInfo(name = "valor_enviado") var valor_eviado: Int
)