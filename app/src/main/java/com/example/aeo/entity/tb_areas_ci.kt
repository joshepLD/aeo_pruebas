package www.tasisoft.com.db

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_areas_ci_entity")
data class tb_areas_ci(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name =  "id")
    var id: Int = 0,
    @ColumnInfo(name = "tipo")
    var tipo: String = "",
    @ColumnInfo(name = "destino")
    var destino: String = "",
    @ColumnInfo(name = "horario")
    var horario: String = "",
    @ColumnInfo(name = "direccion")
    var direccion: String = ""
)