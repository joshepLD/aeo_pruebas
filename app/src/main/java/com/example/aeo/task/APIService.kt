package com.example.aeomovil.task

import com.example.aeo.model.response.*
import com.example.aeomovil.model.response.CartaInstruccionesResponse
import com.example.aeomovil.model.response.LoginResponse
import com.example.aeomovil.model.response.TurnoResponse
import com.example.aeomovil.model.response.ValidarTurnoResponse
import retrofit2.Call
import retrofit2.http.*
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectInput
import java.time.LocalDateTime

interface APIService {
    //Version 1.0 del Webservice Login
    @GET("ws_scp.php?funcion=validaUsrSmph")
    fun login(@Query("usr") usuario:String,
              @Query("psw") password:String,
              @Query("imei") imei:String) : Call<LoginResponse>

    //Version 1.0 del Webservice Solicitud de Turno
    @GET("ws_scp.php?funcion=validaSolicitudTurnoOperador")
    fun solicitudTurno(@Query("idUsuarioOperador") idUsiario:String) : Call<TurnoResponse>

    //Version 1.0 del Webservice Validacion de Solicitud de Turno
        @GET("ws_scp.php?funcion=creaRegistroSolicitud")
    fun validarTurno(@Query("fechaSolicitud") fechaSolicitud: LocalDateTime,
                     @Query("idUsuarioOperador") idUsuarioOperador:String,
                     @Query("idEquipo") idEquipo:String) : Call<ValidarTurnoResponse>

    @GET("get_ci_g.php?")
    fun cartaInstrucciones(@Query("id_operador") idOperador:String) : Call<CartaInstruccionesResponse>

    @GET("WSTutorials.php?")
    fun tutoriales(@Query("id_operador") idOperador:String) : Call<TutorialesResponse>

    @GET("WSShiftRequestStatus.php?")
    fun turno(@Query("id_operador") idOperador:String) : Call<VerificarTurnoResponse>

    //Version 1.0 del Webservice
    @GET("WSGetAdvancePayment.php?")
    fun anticiposDos(@Query("id_operador") idOperador:String) : Call<AnticiposResponse>

    //Version 2.0 del Webservice
    @GET("WSGetAdvancePaymentBank.php?")
    fun anticipos(@Query("id_operador") idOperador:String) : Call<AnticiposResponse>


    @GET("WSTravelLiquidation.php?")
    fun liquidacion(@Query("id_operador") idOperador:String) : Call<LiquidacionResponse>

    //Enviod de evidencias 1.0
    @GET("WSUploadFile.php")
    fun envioGaleria(@Query("id_operador") idOperador:String,
                     @Query("id_carta") idCarta:String,
                     @Query("folio_carta") folioCarta:String,
                     @Query("tipo_carta") tipoCarta:String,
                     @Query("tipo_evidencia") tipoEvidencia:String,
                     @Query("id_evidencia") idEvidencia:String,
                     @Query("evidence") evidence: FileOutputStream,
                     @Query("comentario") comentario:String) : Call<EnvioGaleriaResponse>

    //Enviod de evidencias 2.0
    @FormUrlEncoded
    @POST("WSUploadFileType.php")
    fun envioGaleriaB(@Field("id_operador") idOperador:String,
                      @Field("id_carta") idCarta:String,
                      @Field("folio_carta") folioCarta:String,
                      @Field("tipo_carta") tipoCarta:String,
                      @Field("tipo_evidencia") tipoEvidencia:String,
                      @Field("id_evidencia") idEvidencia:String,
                      @Field("evidence") evidence: String,
                      @Field("tipo_archivo") tipoArchivo: String,
                      @Field("comentario") comentario:String) : Call<EnvioGaleriaResponseB>

    //Enviod de evidencias 2.0
    @FormUrlEncoded
    @POST("WSUploadFileTypeArray.php")
    fun envioGaleriaArray(@Field("dataGalery") arrayImages: String) : Call<EnvioGaleriaResponseArray>

    //Version 1.0 del Webservice Lectura de CI

    @GET("WSUpdateViewCI.php?")
    fun LeidoCI(@Query("id_carta") idCarta:String,
                @Query("tipo_ci") tipoCI:String,
                @Query("fecha") fecha:String,
                @Query("tipo") tipo:String) : Call<LeidoResponse>

}





